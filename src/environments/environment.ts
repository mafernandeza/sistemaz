// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBCZIapH2kOl42Auon8Pwy8seCMFDVkd7M",
    authDomain: "sistemaz.firebaseapp.com",
    databaseURL: "https://sistemaz.firebaseio.com",
    projectId: "sistemaz",
    storageBucket: "sistemaz.appspot.com",
    messagingSenderId: "395748520475",
    appId: "1:395748520475:web:55087462e33c4b2b2d3ebe"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
