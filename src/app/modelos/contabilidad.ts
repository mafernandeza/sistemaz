import { NodeCompatibleEventEmitter } from 'rxjs/internal/observable/fromEvent';
 
  export interface entidadEgreso{
    IdEntidadEgreso ?: number ;
    Nombres ?: string;
    ApPaterno? : string;
    ApMaterno? : string;
    Celular? : string;
    Correo? : string;
    PagoPeriodico? : boolean;
    DiaPagoPeriodico? : number; 
    MontoPagoPeriodico? : number;
  }