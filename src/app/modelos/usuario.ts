export interface usuario {
  idUsuario ? : number; 
  idPerfil? : number;
  idSede? : number;
  
  UsuarioSistema? : string;

  ContraseniaNueva? : string;
  ContraseniaAnt? : string;

  Nombres? : string; 
  ApPaterno? : string; 
  ApMaterno? : string;
  Correo? :string;
  nPerfil? : string;
  nSede? : string;
}
  
export interface perfil{
  idPerfil? : number ;
  nombre? : string ;
}

export interface sede{
  idSede? : number;
  Nombre? : string;
  Detalle? : string;
}