export interface unidadMedida {
  idUnidadMedida?: number;
  unidadMedida?: string;
}

export interface producto{
  IdUnidadMedida ? : number;
  idProducto ? : number; 
  IdUsuario ? : number; 
  idMoneda ? : number;
  idPedido? : number; 
  idCategoriaProducto ? : number; 

  nomProducto ? : string; 
  stock ? : number ; 
  costoCompras? : number;
  costoGasto? : number; 

  PrecioUnidad ? : number; 
  PrecioTotal ? : number; 
  nombreCategoria ? : string;
  simboloMoneda? : string;
  nombreUnidadMedida? : string; 
  fechaCompra? : Date; 
}

export interface productoBuscar{
  idCategoriaProducto? : number;
  nomProducto? : string; 
  ordeby? : boolean; 
  idSede? : number;

  numPag? :number;
  tamPag? : number;
}

export interface categoriaProducto {
  idCategoriaProducto? : number; 
  Nombre? : string;
}

export interface pedido{
  idPedido? : number; 
  NombrePedido? : string;
  FechaPedido? : string;
  FechaPedidoDT? : Date;
  costoTotal? : string; 
  idSede? : number; 
}

export interface compraProducto{
  idCompraProducto? : number;
  Cantidad? : number ; 
  Precio? : number; 
  nombreProducto? : string;
}