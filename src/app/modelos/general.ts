export interface moneda {
  idMoneda ? : number;
  codigo?: string;
  nombre?: string;
  simbolo ? : string;
}
