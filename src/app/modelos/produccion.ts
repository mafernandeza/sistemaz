export interface consumo {
  IdConsumo: number;
  Fecha ? : string;
  FechaDT ? : Date;
  Nombre ? : string;
  costoTotal ? : string;
  }

 export interface detalleConsumo{  
  idProducto? :  number; 
  idConsumo? : number;
  cantidad? : number ; 
  producto? : string;
  costoTotal? : string; 
 } 