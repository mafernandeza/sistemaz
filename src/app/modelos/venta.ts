export interface paramsListaMenu {
  idSede? : number ; 
  nombreProducto? : string; 
  costoMaximo : number ;
  tPag? : number ;
  nPag? : number;
}

export interface producto{
  
    idProducto?:  number;
    idUnidadMedida?: number;
    idUsuario: number;
    idSede: number;
    idMoneda: number;
    nombre: string;
    stock?:number;
    precio? : number;
    SimboloMoneda : string;
}
  