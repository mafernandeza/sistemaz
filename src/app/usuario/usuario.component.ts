import { Component, OnInit } from '@angular/core';
import { constants } from 'os';
import { perfil, sede, usuario } from '../modelos/usuario';
import { GlobalService } from '../services/global.service';
import { UsuarioService } from '../services/servUsuario/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {
  lstUsuarios : usuario[] = null; 
  lstPerfil : perfil[] = null;
  lstSede : sede[]= null;
  mdUsuario : boolean = false;
  msjUsuario : string = 'Nuevo usuario';

  constructor( private usuario : UsuarioService, private global : GlobalService) { }

  ngOnInit() {
    
    this.listaUsuarios(); this.listarPerfiles(); this.listarSede();
  }
  listarSede(){
    this.usuario.servListarSede().subscribe(
      (result : sede[])=>{
        this.lstSede  = result;
        console.log(this.lstSede);
      }
    );
  }
  listarPerfiles(){
    this.usuario.servListarPerfil().subscribe(
      (result : perfil[])=>{
        this.lstPerfil  = result;
        //console.log(this.lstPerfil);
      }
    );
  }

  listaUsuarios(){
    this.usuario.servlistaUsuarios().subscribe(
      (result : usuario[])=>{
        this.lstUsuarios = result;
        //console.log(this.lstUsuarios);
      }
    );
  }

  usuarioEditando : usuario = {} ;
  editarUsuario(idus: number,nomb : string, apPat :string, apMat :string, correo: string, idPerf :number, idSed : number, us:string ){
    this.mdUsuario = true; this.msjUsuario = 'Editar usuario';
    this.usuarioEditando.Nombres = nomb; this.usuarioEditando.ApPaterno = apPat; this.usuarioEditando.ApMaterno = apMat;
    this.usuarioEditando.Correo = correo; this.usuarioEditando.idPerfil = idPerf; this.usuarioEditando.idSede = idSed;
    this.usuarioEditando.UsuarioSistema = us; this.usuarioEditando.ContraseniaNueva = ''; this.usuarioEditando.idUsuario = idus;
    this.usuarioEditando.ContraseniaAnt = ''; this.usuarioEditando.ContraseniaNueva ='';
    //console.log(nomb, this.usuarioEditando.Nombres);
  }
  nuevoUsuario(){
    this.mdUsuario = true; this.msjUsuario = 'Nuevo usuario';
    this.usuarioEditando = { idUsuario:0,ContraseniaNueva:'', ContraseniaAnt :''};
  }
  insertarEditarUsuario(){
    //console.log(this.usuarioEditando);
    
    this.usuarioEditando.idSede = parseInt( this.usuarioEditando.idSede.toString() );    
    this.global.detalle_login.idSede = parseInt( this.usuarioEditando.idSede.toString() );
    sessionStorage.setItem("usLog" , JSON.stringify(this.global.detalle_login));

    this.usuarioEditando.idPerfil = parseInt( this.usuarioEditando.idPerfil.toString() ); 


    this.usuario.servInsertarEditarUsuario(this.usuarioEditando).subscribe(
      (result: boolean)=>{
        if(result == true){
          this.mdUsuario = false;
        }
        else{
          this.mdUsuario = false;
          alert('La contraseña no se ha modificado!');
        }
        this.listaUsuarios();
      }
    );
  }

}
