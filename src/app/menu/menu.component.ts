import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {detalle_login, postDataLogin} from 'src/app/modelos/detalle_persona';
import {menu} from 'src/app/modelos/menu';
import {GlobalService} from 'src/app/services/global.service';
import {MenuService} from 'src/app/services/servMenu/menu.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css',
  //'./font-awesome.min.css'
  ]
})
export class MenuComponent implements OnInit {

  dataUsuarioLog : detalle_login ;
  peticionMenu : boolean  = false;
  listaMenu  : menu[]  = null; 


  constructor(private router : Router, private globlal: GlobalService, private menu : MenuService ) {
    
  }

  ngOnInit() { 
    this.globlal.setDetalle_login();
    this.evaluarMenu();
  }

  evaluarMenu(){
    this.dataUsuarioLog = this.globlal.detalle_login;
    if(sessionStorage.getItem("menu") == null){
      this.menu.obtenerMenu(this.globlal.detalle_login.idPerfil).subscribe(
        (res :  menu [] ) =>{
          this.listaMenu =res;
          //console.log(this.listaMenu);
          sessionStorage.setItem("menu",JSON.stringify(res));
          //console.log('menú obtenido de servicio'); 
        }
      );
    }
    else{
      //console.log('menú obtenido de session'); 
      this.listaMenu =JSON.parse(sessionStorage.getItem("menu")) ;
    }
  }

  evaluarMenu_ant(){
    console.log(this.dataUsuarioLog);
    
    //this.dataUsuarioLog = JSON.parse(sessionStorage.getItem("menuActual")) ;
    if(this.globlal.detalle_login ){
      //console.log('datos de usuario ---> global');
      this.dataUsuarioLog = this.globlal.detalle_login;
    }  
    else{
      //console.log('datos de usuario ---> sesion');
      this.dataUsuarioLog = JSON.parse(sessionStorage.getItem("usLog"));
      this.globlal.detalle_login = this.dataUsuarioLog; 
    } 
    

    if(sessionStorage.getItem("menu") == null){
      this.menu.obtenerMenu(this.dataUsuarioLog.idPerfil).subscribe(
        (res :  menu [] ) =>{
          this.listaMenu =res;
          sessionStorage.setItem("menu",JSON.stringify(res));
          //console.log('menú obtenido de servicio'); 
        }
      );
    }
    else{
      //console.log('menú obtenido de session'); 
      this.listaMenu =JSON.parse(sessionStorage.getItem("menu")) ;
    }
    console.log(this.dataUsuarioLog); 
  }
  
  
  cerrarSession(){
    sessionStorage.removeItem("usLog");
    sessionStorage.removeItem("menu");
    this.router.navigate([''] ); 
  }
}
