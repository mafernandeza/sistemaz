import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { almacenComponent } from './almacen.component';

describe('ProductoComponent', () => {
  let component: almacenComponent;
  let fixture: ComponentFixture<almacenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ almacenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(almacenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
