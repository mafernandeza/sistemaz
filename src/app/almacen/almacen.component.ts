import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { unidadMedida, producto, categoriaProducto, pedido,compraProducto, productoBuscar } from '../modelos/productoModelos';
import { GlobalService } from '../services/global.service';
import { $, ProtractorExpectedConditions } from 'protractor';
import { moneda } from '../modelos/general';
import { almacenService } from '../services/servAlmacen/producto.service';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-almacen',
  templateUrl: './almacen.component.html',
  styleUrls: ['./almacen.component.css']
})
export class almacenComponent implements OnInit {

  listaUnidadMedida : unidadMedida[] = null; 
  listaTipoMeneda : moneda[] = null;
  listaProducto : producto [] = null ;
  listaCategoriaProducto : categoriaProducto[] = null;
  listaPedido : pedido[] = null;

  mdlIsOpen : boolean = false;
  mdlIsOpenCompra: boolean = false;
  existePedido : boolean = false;

  product : producto  = {nomProducto :'' ,IdUnidadMedida :1, idCategoriaProducto: 1};
  filaSelect : number = null; 
  filaSelectPedido : number  = null; 
  productCompra : producto = {nomProducto :'Debe elegir un producto', idMoneda :1 }; // utilizado para registar una compra de un producto
  productoBusar : productoBuscar = {nomProducto:'', idCategoriaProducto:0, ordeby : false,numPag:1,tamPag:10 }
  pagProducto : number[] ;

  constructor(private router : Router, private almacen : almacenService, private global : GlobalService) { }

  ngOnInit() {
    
    //general
    this.obtenerListaUnidadMedidas();
    //this.listarTipoMoneda();
    // genera
    this.obtenerCantidadPaginasProducto();
    this.obtenerProductos(1);
    this.obtenerCategoriasProducto();
    this.listarPedidos();
  }

  obtenerCantidadPaginasProducto(){
    this.almacen.listaProductosContar(this.productoBusar).subscribe(
      (r : number)=>{
        
        var cantidadP = (r / 10)  ;
        cantidadP = Math.trunc(cantidadP);
        if((r % 10) > 0 )cantidadP ++ ;
        
        
        var temp = []
        for(var i = 0 ; i< cantidadP; i++){
          temp.push(i+1);         
        }
        this.pagProducto = temp;
        
      }
    );
  }

  cerrarModalNProduct (){
    this.mdlIsOpen = false;
  }
  //general
  obtenerListaUnidadMedidas(){
    this.almacen.listarUnidadMedida().subscribe(
      (res : unidadMedida[]) =>{
        this.listaUnidadMedida = res;
        //console.log(this.listaUnidadMedida);
      }
    );
  }

  //general
  listarTipoMoneda(){
    this.almacen.listarTipoMoneda().subscribe(
      (res : moneda[]) =>{
        this.listaTipoMeneda = res;
        
      }
    );
  }

  reiniciarProducto(){
    this.mdlIsOpen = true;
    this.product.nomProducto = '';
    this.product.IdUnidadMedida = 1;
    this.product.PrecioUnidad = 0;
    this.product.PrecioTotal = 0;
    this.product.idMoneda = 1;
    this.product.idCategoriaProducto = 1;
    this.product.idProducto = 0;
  }

  obtenerProductos(pag : number){
    this.global.setDetalle_login();
    this.productoBusar.idSede = this.global.detalle_login.idSede;
    this.productoBusar.idCategoriaProducto = parseInt(this.productoBusar.idCategoriaProducto.toString());

    if(pag == -1){this.productoBusar.numPag = this.pagProducto.length; }
    
    else{this.productoBusar.numPag = pag;}

    //console.log(this.productoBusar);
    this.almacen.listaProductos(this.productoBusar).subscribe(
      (res: producto[]) =>{
        this.listaProducto = res;
        //console.log(this.listaProducto);
      }
    );
  }

  obtenerCategoriasProducto(){
    this.almacen.listaCategoriasProductos().subscribe(
      (res: categoriaProducto[])=>{
        this.listaCategoriaProducto = res;
      }
    );
  }

  editarProducto(idProd,nomProd,idCategoriaProd,idUnidadMed, precio,idMoneda){
    this.mdlIsOpen = true;
    this.product.nomProducto = nomProd; 
    this.product.idProducto = idProd;
    this.product.idCategoriaProducto =(idCategoriaProd);
    this.product.IdUnidadMedida =(idUnidadMed);    
    this.product.PrecioUnidad = precio ;
    this.product.idMoneda =  (idMoneda );
    //console.log(this.product);
  }

  insertarProducto(){
    this.product.IdUsuario = this.global.detalle_login.idUsuario; 
    //this.product.IdRazonSocial = this.global.detalle_login.idRazonSocial;
    
    this.product.IdUnidadMedida = parseInt(this.product.IdUnidadMedida.toString())  ;    
    this.product.idCategoriaProducto = parseInt(this.product.idCategoriaProducto.toString());
    this.product.idMoneda = 1 ;//1 --> SOL //parseInt(this.product.idMoneda.toString());
    this.product.PrecioUnidad = 0; 
    
    if(this.product.nomProducto == ''){
      alert('Completar con nombre de producto.');
      return false;
    }
    console.log(this.product);
    this.almacen.insertarProducto(this.product).subscribe(
      (res: boolean) =>{
         if(res == true){
          //document.getElementById('btnCerrarModalProducto').click(); 
          this.mdlIsOpen = false;
          this.obtenerCantidadPaginasProducto();
          this.obtenerProductos(this.productoBusar.numPag);
          
         }
         else{
           alert('Ocurrio un error.');
           //document.getElementById('btnCerrarModalProducto').click();
           this.mdlIsOpen = false;
         }
      }
    );
  }

  
  RowSelected(idProducto: number, numFila : number, nomProducto : string, precioAnt : number, simboloMon : string, idMoneda : number){
    if(this.existePedido == false){
      alert('Primero de guardar un pedido'); return false;
    }
    this.mdlIsOpenCompra = true;
    this.filaSelect = numFila;
    
    this.productCompra.nomProducto = nomProducto;
    this.productCompra.idProducto = idProducto;
    this.productCompra.simboloMoneda = simboloMon;
    this.productCompra.idMoneda = idMoneda;
    this.productCompra.PrecioUnidad = precioAnt; // precio --> el anterior registrado
    //console.log(precioAnt );
   
  }

  calcularPrecio(){
    if(this.productCompra.stock < 0){this.productCompra.stock = 0;}
    this.productCompra.PrecioTotal = (this.productCompra.PrecioUnidad * this.productCompra.stock)  ;
  }

  insertarCompra(){
    if(this.productCompra.idProducto == null){
      alert('Debe elegir un producto primero');
      return false;
    }

    this.productCompra.idMoneda = parseInt( this.productCompra.idMoneda.toString() );
    this.productCompra.idPedido = parseInt( this.pedidoActual.idPedido.toString());
    this.productCompra.fechaCompra = new Date();
    
    this.almacen.insertarCompra(this.productCompra).subscribe(
      (res: boolean) => {
        
        if(res == true){
          this.productCompra.PrecioUnidad = null;
          this.productCompra.stock = null;
          this.obtenerProductos(this.productoBusar.numPag);
          this.obtenerProductosPorPedido(this.pedidoActual.idPedido,this.filaSelectPedido,this.pedidoActual.NombrePedido);
          this.listarPedidos();
        }
        else{
          alert('Ocurrio un error');
          this.obtenerProductos(this.productoBusar.numPag);
        }
        this.mdlIsOpenCompra = false;
      }
    );

  }


  listarPedidos(){
    this.almacen.listarPedidos().subscribe(
      (res: pedido[])=>{
        this.listaPedido = res;
        //console.log(this.listaPedido);
      }
    );
  }

  pedidoActual: pedido = {NombrePedido:'',FechaPedidoDT: new Date(), idSede : 0};
  nuevoEditantoPedido : string = "Nuevo pedido: ";
  guardarPedido(){
    this.existePedido = true;//this.pedidoActual.NombrePedido;
    this.pedidoActual.idSede = this.global.detalle_login.idSede;
    this.nuevoEditantoPedido = "Editando Pedido: ";
    
    //console.log(this.pedidoActual );
    if(this.pedidoActual.idSede == 0){alert('Sede no identificada'); return false;}
    this.almacen.insertarPedido(this.pedidoActual).subscribe(
      (idPedidoGen : number) =>{
        //console.log(idPedidoGen);
        this.pedidoActual.idPedido = idPedidoGen;
        
        this.listarPedidos();
        this.filaSelectPedido = 0;
      }
    );
  }

  litaProductosPedito : compraProducto[] = null; 
  obtenerProductosPorPedido(idPedido : number, i : number, nomPedid : string){
    
    this.filaSelectPedido = i;
    this.nuevoEditantoPedido = "Editando Pedido: ";
    //console.log(idPedido);
    this.existePedido = true;
    //this.nuevoPedidoCk = false;
    
    this.pedidoActual.NombrePedido = nomPedid ;
    this.pedidoActual.idPedido  = idPedido;

    this.almacen.listaCompraProductoPorPedido(idPedido).subscribe(
      (lst : compraProducto[])=>{ 
        this.litaProductosPedito  = lst;
        //console.log(this.litaProductosPedito);
        //this.obtenerProductosPorPedido(this.pedidoActual.idPedido,this.filaSelectPedido,this.pedidoActual.NombrePedido);
      }
    );
  }

  //nuevoPedidoCk : boolean  = true;
  nuevoPedido (){
    
    this.existePedido = false;
    console.log(this.existePedido);
    //this.filaSelectPedido = -1;
    this.pedidoActual.NombrePedido = '';
    this.pedidoActual.idPedido = 0;
    this.nuevoEditantoPedido = "Nuevo pedido: ";
  }
}
