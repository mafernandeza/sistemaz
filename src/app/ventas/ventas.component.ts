import { Component, OnInit } from '@angular/core';
import { VentaService } from '../services/servVenta/venta.service';
import { paramsListaMenu, producto } from '../modelos/venta';
import { GlobalService } from '../services/global.service';
import { MenuComponent } from '../menu/menu.component';

@Component({
  selector: 'app-ventas',
  templateUrl: './ventas.component.html',
  styleUrls: ['./ventas.component.css']
})
export class VentasComponent implements OnInit {

  params  :  paramsListaMenu =  {nPag:0,tPag :10, costoMaximo: 100, idSede : -1, nombreProducto:""}; 
  lstProducto : producto[] = null; 
  filaSelect : number = null; 
  
  unidadMedidaSelectt : string ="";
  stockSelect : number = 0;
  productoSelect : string ="";

  cantidadAgregar : number = 0;

  constructor(private sVenta : VentaService, private global : GlobalService) { }

  ngOnInit() {
    this.global.setDetalle_login();
    this.ObtenerProductos();
  }

  ObtenerProductos(){
    //this.params.idRazonSocial = this.global.detalle_login.idRazonSocial; 
    //console.log(this.params);
    this.sVenta.listaProductos(this.params).subscribe(
      (res : producto[])=>{
       this.lstProducto = res;
      
      }
    ); 
  }

  idProductoSelect : number = 0;
  Agregar_a_Detalle(){
    if(this.cantidadAgregar<= this.stockSelect && this.cantidadAgregar >0){
      //console.log('agregar idProducto: '+ this.idProductoSelect+'cantidad: '+this.cantidadAgregar);
      this.ocultarModal('btnCerrarModalCantidadCompra');
      var html = document.getElementById('tbDetalleVenta').innerHTML;
          html += '<tr>'+ '<td hidden>'+this.idProductoSelect+'</td><td>'+this.productoSelect+'</td><td>'+this.cantidadAgregar+'</td>' +'</tr>';
      var tb = document.getElementById('tbDetalleVenta');
      tb.innerHTML = html;
      this.cantidadAgregar = 0;
    }else{
      //alert('La cantidad excede al Stock disponible actualmente.');
      alert('Cantidad no valida.');
    }
      //console.log(this.cantidadAgregar);
  }

  RowSelected(idProducto: number, numFila : number, unidadMedida : string, stock : number, nombre : string){
    this.idProductoSelect = idProducto;
    this.filaSelect = numFila;
    this.unidadMedidaSelectt = unidadMedida;
    this.stockSelect = stock;
    this.productoSelect = nombre;
  }

  
  ocultarModal(NombreModal){
    //$('#ModalCantidadCompra').modal('hide');
    var btn = document.getElementById(NombreModal);
    //modal.style.display = 'none';
    //modal.className = 'modal fade';
    btn.click();
    
  }
}
