import { typeWithParameters } from '@angular/compiler/src/render3/util';
import { Component, OnInit } from '@angular/core';
import { entidadEgreso } from '../modelos/contabilidad';
import { ContabilidadService } from '../services/servContabilidad/contabilidad.service';

@Component({
  selector: 'app-entidad-egreso',
  templateUrl: './entidad-egreso.component.html',
  styleUrls: ['./entidad-egreso.component.css']
})
export class EntidadEgresoComponent implements OnInit {

  mdlpen : boolean = false;
  entEEditarNuevo : entidadEgreso = {IdEntidadEgreso: 0}
  lstEntEgreso : entidadEgreso[] = null ;

  constructor(private contabilidad : ContabilidadService) {}

  ngOnInit() {
    this.listarEntidadEgreso();
  }

  nuevo(){
    this.entEEditarNuevo.IdEntidadEgreso = 0; 
    this.mdlpen = true;

    this.entEEditarNuevo.Nombres = ''; 
    this.entEEditarNuevo.ApPaterno = '';
    this.entEEditarNuevo.ApMaterno = ''; 
    this.entEEditarNuevo.Celular = null;
    this.entEEditarNuevo.Correo = '';
    this.entEEditarNuevo.PagoPeriodico = false;
    this.entEEditarNuevo.DiaPagoPeriodico = null; 
    this.entEEditarNuevo.MontoPagoPeriodico = null;
    
  }

  guardar(){
    console.log(this.entEEditarNuevo);
    this.contabilidad.servInsertarEditarEntEg(this.entEEditarNuevo).subscribe(
      (result : entidadEgreso)=>{
        console.log(result); 
        this.listarEntidadEgreso();
        this.mdlpen = false;
      }
    );
  }
  
  listarEntidadEgreso(){
   this.contabilidad.servListarEntEg().subscribe(
    (result: entidadEgreso[])=>{
      this.lstEntEgreso= result;
      //console.log(result);
    });     
   }
  

  
  activarCamposPagoPeriodico(){
    if(this.entEEditarNuevo.PagoPeriodico == true){
      this.entEEditarNuevo.PagoPeriodico = false;
      this.entEEditarNuevo.MontoPagoPeriodico = null;
      this.entEEditarNuevo.DiaPagoPeriodico = null;
    }
    else{
      this.entEEditarNuevo.PagoPeriodico = true;
    }
    
  }
  editar(idEntEg : number, nombres: string, apPaterno: string, apMaterno: string, correo :string, 
    celular: string, diaPagoPeriodico : number, montoPagoPeriodico: number, pagoPeriodico : boolean){
    this.entEEditarNuevo.IdEntidadEgreso = idEntEg; 
    this.entEEditarNuevo.Nombres = nombres; this.entEEditarNuevo.Correo = correo; 
    this.entEEditarNuevo.ApPaterno = apPaterno; this.entEEditarNuevo.Celular = celular;
    this.entEEditarNuevo.ApMaterno = apMaterno; this.entEEditarNuevo.DiaPagoPeriodico = diaPagoPeriodico;
    this.entEEditarNuevo.MontoPagoPeriodico = montoPagoPeriodico; 
    this.entEEditarNuevo.PagoPeriodico = pagoPeriodico; 
    
    this.mdlpen = true;
  }
}
