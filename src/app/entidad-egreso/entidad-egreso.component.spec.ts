import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntidadEgresoComponent } from './entidad-egreso.component';

describe('EntidadEgresoComponent', () => {
  let component: EntidadEgresoComponent;
  let fixture: ComponentFixture<EntidadEgresoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntidadEgresoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntidadEgresoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
