import { Summary } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { consumo, detalleConsumo } from '../modelos/produccion';
import { unidadMedida, producto, categoriaProducto, pedido,compraProducto, productoBuscar } from '../modelos/productoModelos';
import { GlobalService } from '../services/global.service';
import { almacenService } from '../services/servAlmacen/producto.service';
import { ProduccionService } from '../services/servProduccion/produccion.service';


@Component({
  selector: 'app-produccion',
  templateUrl: './produccion.component.html',
  styleUrls: ['./produccion.component.css']
})
export class ProduccionComponent implements OnInit {
  listaProducto : producto [] = null ;
  productoBusar : productoBuscar = {idCategoriaProducto: 0, ordeby : true, nomProducto : '',tamPag:10}  ;
  pagProducto : number[] ;
  listaCategoriaProducto : categoriaProducto[] = null;
  listaConsumo : consumo[] = null; 
  listaUnidadMedida : unidadMedida[] = null;

  filaSelectProducto : number  = null;
  consumoInsert : consumo = {IdConsumo : 0,FechaDT : new Date(), Nombre:''};

  mdlPedir : boolean  = false;
  productoSelect : string = "";
  cantidadProdutoSelect : number = 0;
  //existePedido : boolean = false;
  
  mensajePedido : string = 'Nuevo pedido';
  
  constructor(private almacen : almacenService, private produccion : ProduccionService, private global : GlobalService ) { }

  ngOnInit() {
    this.obtenerCategoriasProducto();
    
    this.obtenerCantidadPaginasProducto();
    this.obtenerProductos(1);
    this.listarConsumos(); 
    this.obtenerListaUnidadMedidas();
  }

  obtenerListaUnidadMedidas(){
    this.almacen.listarUnidadMedida().subscribe(
      (res : unidadMedida[]) =>{
        this.listaUnidadMedida = res;
        //console.log(this.listaUnidadMedida);
      }
    );
  }

  obtenerCantidadPaginasProducto(){
    this.almacen.listaProductosContar(this.productoBusar).subscribe(
      (r : number)=>{
        var cantidadP = (r / 10)  ;
        cantidadP = Math.trunc(cantidadP);
        if((r % 10) > 0 )cantidadP ++ ;
        
        var temp = []
        for(var i = 0 ; i< cantidadP; i++){
          temp.push(i+1);         
        }
        this.pagProducto = temp;
      }
    );
  }

  obtenerProductos(pag :number){
    this.global.setDetalle_login();
    this.productoBusar.idCategoriaProducto = parseInt(this.productoBusar.idCategoriaProducto.toString());
    this.productoBusar.idSede = this.global.detalle_login.idSede; 
    
    if(pag == -1){this.productoBusar.numPag = this.pagProducto.length; }
    else{this.productoBusar.numPag = pag;}

    //console.log(this.productoBusar);
    this.almacen.listaProductos(this.productoBusar).subscribe(      
      (res: producto[]) =>{
        this.listaProducto = res;
        //console.log(this.listaProducto);
      }
    );
  }
  obtenerCategoriasProducto(){
    this.almacen.listaCategoriasProductos().subscribe(
      (obj: categoriaProducto[])=>{
        this.listaCategoriaProducto = obj;
      }
    );
  }

  listarConsumos(){
    this.produccion.listaConsumo().subscribe(
      (obj: consumo[]) =>{
          this.listaConsumo = obj; 
          //console.log(this.listaConsumo);
      }
    );
  }

  insertarDetalleConsumo : detalleConsumo = {cantidad : 0, idConsumo : -1}
  idUnidadMedidaSelect : number  = -1;
  regConsumoProducto( idProducto: number, numFila : number, nomProducto : string, precio : number, simboloMon : string, idMoneda : number, cantidad : number, idUnidadMedidaSel : number){
    if(this.insertarDetalleConsumo.idConsumo == -1){
      alert('Primero debe seleccionar un pedido!');
      return false;
    }
    
    this.filaSelectProducto = numFila;
    this.productoSelect = nomProducto;
    this.cantidadProdutoSelect = cantidad;
    this.mdlPedir = true;
    this.idUnidadMedidaSelect  = idUnidadMedidaSel;
    this.insertarDetalleConsumo.idProducto = idProducto;
    //console.log(numFila, idProducto, nomProducto, precio, simboloMon, idMoneda);
  }

  insertarConsumo(){
    this.produccion.insertarConsumo(this.consumoInsert).subscribe(
      (result : number)=>{
        //console.log('idGen: ' + result);
        this.listarConsumos();
        //this.idConsumoActual = result;
        this.consumoInsert.IdConsumo = result;
        this.mensajePedido = "Editar pedido";
      }
    );
  }

  filaSelectConsumo : number = null;
  listaDetalleConsumo : detalleConsumo[] = null;

  obtenerDetallePorPedido(idConsumo:number , i :number, nomConsumo : string ){
    this.filaSelectConsumo = i;    
    this.mensajePedido = "Editar pedido";    
    this.consumoInsert.Nombre = nomConsumo;
    this.consumoInsert.IdConsumo = idConsumo;
    this.insertarDetalleConsumo.idConsumo = idConsumo;

    this.produccion.servdetalleConsumo(this.consumoInsert).subscribe(
      (result : detalleConsumo[] ) =>{
        this.listaDetalleConsumo = result;
        //console.log(this.listaDetalleConsumo);
      }
    );
  }
  nuevoPedido (){
    this.mensajePedido = 'Nuevo pedido';
    this.consumoInsert.Nombre = '';
    this.consumoInsert.IdConsumo = 0;
    this.filaSelectConsumo = -1;
  }
  txtPedir (){
    if(this.insertarDetalleConsumo.cantidad < 1){ 
      this.insertarDetalleConsumo.cantidad = 1; 
      alert('no puedo ingresar numeros negativos'); 
    }
    if(this.insertarDetalleConsumo.cantidad > this.cantidadProdutoSelect){ 
      this.insertarDetalleConsumo.cantidad = this.cantidadProdutoSelect; 
      alert('No puede pedir cantidad superior al Stock');
    }
  }

  guardarDetalleConsumo(){

    
    if(this.insertarDetalleConsumo.cantidad < 1){ alert('Debe ingresar un numero mayor a 0'); return false;}

      this.produccion.servinsertarDetalleConsumo(this.insertarDetalleConsumo).subscribe(
        (result : boolean)=>{
          console.log(result);
          if(result == true){
              this.obtenerDetallePorPedido(this.consumoInsert.IdConsumo, this.filaSelectConsumo, this.consumoInsert.Nombre);
              this.obtenerProductos(1);
              this.listarConsumos();
              this.mdlPedir = false;
              this.insertarDetalleConsumo.cantidad = 0;
          }

          else{alert('Ocurrio un problema :(');}
        }
      );
  }

}
