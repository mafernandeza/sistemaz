import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalService } from '../global.service';
import { paramsListaMenu } from 'src/app/modelos/venta';

@Injectable({
  providedIn: 'root'
})
export class VentaService {

  

  constructor(private http : HttpClient, private global : GlobalService) { }
  
  listaProductos(params : paramsListaMenu){
    
    return this.http.post(this.global.urlServicio+'/Venta/obtenerListaProductos' ,params, this.global.httpOptions); 
  }
}
