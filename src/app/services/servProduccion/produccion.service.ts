import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalService } from '../global.service';
import { consumo, detalleConsumo } from 'src/app/modelos/produccion';

@Injectable({
  providedIn: 'root'
})
export class ProduccionService {

  constructor(private http: HttpClient , private global : GlobalService) { }

  listaConsumo(){
    return this.http.post(this.global.urlServicio+'/producto/listaConsumo', null , this.global.httpOptions); 
  }

  insertarConsumo(param : consumo){
    return this.http.post(this.global.urlServicio+'/producto/insertarConsumo', param , this.global.httpOptions); 
  }

  servdetalleConsumo (param : consumo){
    return this.http.post(this.global.urlServicio+'/producto/detalleConsumo', param , this.global.httpOptions); 
  }

  servinsertarDetalleConsumo (param : detalleConsumo){
    return this.http.post(this.global.urlServicio+'/producto/insertarDetalleConsumo', param , this.global.httpOptions); 
  }
}
