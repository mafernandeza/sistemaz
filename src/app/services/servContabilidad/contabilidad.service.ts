import { Injectable } from '@angular/core';
import { GlobalService } from '../global.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { entidadEgreso } from 'src/app/modelos/contabilidad';

@Injectable({
  providedIn: 'root'
})
export class ContabilidadService {

  constructor(private global : GlobalService, private http: HttpClient ) { }

  servListarEntEg(){
    return this.http.post(this.global.urlServicio+'/contabilidad/listarEntidadEgreso', null , this.global.httpOptions);
  }
  servInsertarEditarEntEg( param :entidadEgreso){
    return this.http.post(this.global.urlServicio+'/contabilidad/insertarEditarEntidad', param , this.global.httpOptions);
  }
  

  servInsertarEgreso(param : any){
    return this.http.post(this.global.urlServicio+'/contabilidad/insertarEgreso', param, this.global.httpOptions);
  }
  servListarEgresos(params: any){
    return this.http.post(this.global.urlServicio +'/contabilidad/listarEgresos', params, this.global.httpOptions); 
  }
  servobtenerEvidencia(params: any){
    return this.http.post(this.global.urlServicio +'/contabilidad/obtenerEvidencia', params, this.global.httpOptions); 
  }
  servListarEgresosContar(params: any){
    return this.http.post(this.global.urlServicio +'/contabilidad/listarEgresosContar', params, this.global.httpOptions); 
  }
  
}
