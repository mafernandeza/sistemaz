import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {AutenticacionService} from 'src/app/services/servAutenticacion/autenticacion.service';
import { GlobalService } from '../global.service';

@Injectable({
  providedIn: 'root'
})
export class MenuGuardGuard implements CanActivate {

  constructor ( private router : Router, private global : GlobalService){}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
        
      if( ! sessionStorage.getItem("usLog")){ return false;}
      else return true;
    
  }
  
}
