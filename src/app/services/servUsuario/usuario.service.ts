import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { usuario } from 'src/app/modelos/usuario';
import { GlobalService } from '../global.service';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor(private http: HttpClient, private global : GlobalService) { }

  servlistaUsuarios(){
    return this.http.post(this.global.urlServicio +'/Usuario/listaUsuarios', null, this.global.httpOptions); 
  }
  servListarPerfil(){
    return this.http.post(this.global.urlServicio + '/Usuario/listaPerfil' , null,this.global.httpOptions);
  }
  servListarSede(){
    return this.http.post(this.global.urlServicio + '/Usuario/listarSede' , null,this.global.httpOptions);
  }
  servInsertarEditarUsuario(param : usuario){
    return this.http.post(this.global.urlServicio + '/Usuario/insertarEditarUsuario' ,param,this.global.httpOptions);
  }
}
