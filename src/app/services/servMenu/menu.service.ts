import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalService } from '../global.service';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient , private global : GlobalService) { }

  

  obtenerMenu(  id : number ){
    
    var  param = {
      idPerfil : id
    }
    
    return this.http.post(this.global.urlServicio+'/home/menuPorPerfil', param , this.global.httpOptions); 
  }
}
