import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { persona } from 'src/app/modelos/persona';
import { Observable, of, from } from 'rxjs';
import {detalle_login, postDataLogin} from 'src/app/modelos/detalle_persona';
import { Interpolation } from '@angular/compiler';
import {GlobalService} from 'src/app/services/global.service'

@Injectable({
  providedIn: 'root'
})
export class AutenticacionService {

  //url: string = 'http://localhost:58534/api/fLogin/autenticacion';
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      "cache-control": "no-cache",
    }) 
  };

  
  constructor(private http: HttpClient , private global : GlobalService ) { }
  
 

  servicoPOST(datax: postDataLogin ){     
    return this.http.post(this.global.urlServicio+'/fLogin/autenticacion', datax , this.httpOptions); 
  } 

  sessionActiva(){
    //console.log('en servicio autent:' , this.global.detalle_login);
    if(this.global.detalle_login == null){ return false;}
    else{ return true;}
    
  }
}