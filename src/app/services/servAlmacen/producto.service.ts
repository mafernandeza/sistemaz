import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { GlobalService } from '../global.service';
import { unidadMedida, producto, pedido, productoBuscar } from '../../modelos/productoModelos';

@Injectable({
  providedIn: 'root'
})
export class almacenService {

  

  constructor(private http: HttpClient, private global : GlobalService) { }

  listarUnidadMedida(){
        
    return this.http.post(this.global.urlServicio+'/Producto/obtenerUnidadMedida' ,null, this.global.httpOptions); 
  }

  listarTipoMoneda(){
    return this.http.post(this.global.urlServicio+'/Producto/listarTipoMoneda' ,null, this.global.httpOptions);
  }


  listaProductos(prod : productoBuscar){
    return this.http.post(this.global.urlServicio+'/Producto/listarProductos',prod,this.global.httpOptions); 
  }
  listaProductosContar(prod : productoBuscar){
    return this.http.post(this.global.urlServicio+'/Producto/listarProductosContar',prod,this.global.httpOptions); 
  }

  listaCategoriasProductos(){
    return this.http.post(this.global.urlServicio+'/Producto/listarCategoriasProducto',null,this.global.httpOptions); 
  }

  insertarProducto(prod : producto){
    return  this.http.post(this.global.urlServicio+'/Producto/insertarProductos',prod,this.global.httpOptions)
  }

  insertarCompra(prod : producto){
    return this.http.post(this.global.urlServicio +'/Producto/insertarCompra', prod, this.global.httpOptions); 
  }

  listarPedidos(){
    return this.http.post(this.global.urlServicio +'/Producto/listarPedidos', null, this.global.httpOptions); 
  }

  insertarPedido(param : pedido){
    return this.http.post(this.global.urlServicio +'/Producto/insertarPedido', param, this.global.httpOptions); 
  }

  listaCompraProductoPorPedido(param : number){
    return this.http.post(this.global.urlServicio +'/Producto/listaCompraProductoPorPedido', param, this.global.httpOptions); 
  }
}
