import { Injectable } from '@angular/core';
import {detalle_login, postDataLogin} from 'src/app/modelos/detalle_persona';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {  servicoURL} from 'src/assets/servicio';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  urlServicio : string = "";
  detalle_login : detalle_login ; 
  
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'      
    }) 
  };
  url_variable : string  = null ;
  

  constructor(private http: HttpClient){
    // 58534  -- 8025
    //this.urlServicio = "https://apiz.azurewebsites.net/api";    
    //this.urlServicio = "http://sistemaz-002-site2.btempurl.com/api";
    //this.urlServicio =  "http://localhost:8025/api";
    //this.urlServicio = "https://738704143bfb.ngrok.io/api";
    this.urlServicio = "http://3.15.18.121/api"
   }

  setDetalle_login(){
    //this.dataUsuarioLog = JSON.parse(sessionStorage.getItem("usLog"));
    //console.log('set uslog');
    this.detalle_login = JSON.parse(sessionStorage.getItem("usLog"));
  }
}