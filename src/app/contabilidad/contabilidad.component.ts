import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable, Subscriber } from 'rxjs';
import { entidadEgreso } from '../modelos/contabilidad';
import { GlobalService } from '../services/global.service';
import { ContabilidadService } from '../services/servContabilidad/contabilidad.service';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { flatMap } from 'rxjs/operators';
@Component({
  selector: 'app-contabilidad',
  templateUrl: './contabilidad.component.html',
  styleUrls: ['./contabilidad.component.css']
})
export class ContabilidadComponent implements OnInit {
  mdlIsOpenNuevo : boolean = false;
  mdlIsOpenZoomImg :  boolean = false;
  mdlLoad : boolean  = false; 
  imgSelect : string = 'assets/img/gif_2.gif';

  lstEntgreso : any = null; 
  lstEgreso : any = null; 
  regEgreso : any = {idEntidadEgreso: 0, idUsuario: 0, fehca : new Date(), monto :0, descripcion:'',evidenciaB64 :''}
  paramBusquedaEgresos : any = { numPag: 1, tamPag: 10, idEntEgre:0,montoDesde: null, montoHasta: null, fDesde :null, fHasta : null}
  
  pagEgresos : number[] ;
  mensajewsp : string = "";

  croppedImage: any = '';
  imageChangedEvent: any = '';

  constructor(private contabilidad : ContabilidadService, private global : GlobalService,  private fbStorage:AngularFireStorage ) { }

  ngOnInit() {
    this.listarEntEgreso();

    
    this.listarEgresos(1);
  }


  sendImage_insertarEgreso(){
    this.mdlLoad = true;
    
    if(this.croppedImage != null){
      const imgID = Date.now();
      const pictur = this.fbStorage.ref('EvidenciaContabilidad/'+imgID+'.jpg').putString(this.croppedImage, 'data_url');
      pictur.then((result)=>{
        //co nsole.log(result);
        this.fbStorage.ref('EvidenciaContabilidad/'+imgID+'.jpg').getDownloadURL().subscribe((url_gen)=>{ 
          //console.log(url_gen);
          this.regEgreso.evidenciaB64 = url_gen;
          this.insertEgreso();
        });
      } ).catch((error)=> {console.log('error enviar imagen',error);})
    }
    else{
      this.insertEgreso();
    }
  }
  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    //console.log(event);
  }
  imageCropped(event: ImageCroppedEvent) {
      this.croppedImage = event.base64;
  }
  imageLoaded() {
      // show cropper
  }
  cropperReady() {
      // cropper ready
  }
  loadImageFailed() {
      // show message
  }

  notificarWsp (celular : string, monto : string, fecha : string, evidencia :string){
    this.mensajewsp = 'https://api.whatsapp.com/send?phone=51' +celular +'&text= Estimado(a), se le notica el depocitado';
    this.mensajewsp += ' de S/. ' + monto + ' en la fecha ' + new Date(fecha).toLocaleString().split(' ')[0];
    this.mensajewsp += "    ; la evidencia puede ver haciendo click en: ";
    window.open(this.mensajewsp, "_blank");
  }
  verEvidencia (val : number){
    this.mdlIsOpenZoomImg = true;
    this.imgSelect= 'assets/img/gif_2.gif';
    var param : any = {idEgreso : val }

    this.contabilidad.servobtenerEvidencia(param).subscribe(
      (result: string)=>{
        this.imgSelect = result;
        
      }
    );
  }

  listarEgresos(pag : number){
    this.paramBusquedaEgresos.numPag = pag;
    
    this.paramBusquedaEgresos.idEntEgre  = parseInt(this.paramBusquedaEgresos.idEntEgre.toString()); 
    if(pag == -1){this.paramBusquedaEgresos.numPag = this.pagEgresos.length; }

    // fechas desde - hasta 
    if(this.paramBusquedaEgresos.fDesde ==  null || this.paramBusquedaEgresos.fHasta == null){      
      this.paramBusquedaEgresos.fDesde = new Date(1999, 0); 
      this.paramBusquedaEgresos.fHasta = new Date(2999, 0); 
    }

    //console.log(this.paramBusquedaEgresos);

    this.contabilidad.servListarEgresos(this.paramBusquedaEgresos).subscribe(
      (result : any)=>{
       this.lstEgreso = result;   
       //console.log(this.lstEgreso);
       this.listarEgresosContar();
      }
    );
  }

  totalMonto : number  = 0 ;
  listarEgresosContar(){
    if(this.paramBusquedaEgresos.fDesde ==  null || this.paramBusquedaEgresos.fHasta == null){      
      this.paramBusquedaEgresos.fDesde = new Date(1999, 0); 
      this.paramBusquedaEgresos.fHasta = new Date(2999, 0); 
    }
    this.contabilidad.servListarEgresosContar(this.paramBusquedaEgresos).subscribe(
      (r : any)=>{
        //console.log(r);
        this.totalMonto = (r.totalMonto).toFixed(2) ; 
        var cantidadP = (r.totalRegitros / 10)  ;
        cantidadP = Math.trunc(cantidadP);
        if((r.totalRegitros % 10) > 0 )cantidadP ++ ;
        
        var temp = []
        for(var i = 0 ; i< cantidadP; i++){
          temp.push(i+1);         
        }
        this.pagEgresos = temp;
        
      }
    );
  }
  keyAnio(){
    this.paramBusquedaEgresos.anio = new Date().getFullYear().toString() ;
    this.paramBusquedaEgresos.mes = new Date().getMonth().toString() ;

    //console.log(this.paramBusquedaEgresos.anio.toString().substring(0,4));
    //this.paramBusquedaEgresos.anio = this.paramBusquedaEgresos.anio.toString().substring(0,4);
    //this.paramBusquedaEgresos.mes = this.paramBusquedaEgresos.mes.toString().substring(0,2);
  }
  NuevoEgreso(){
    this.mdlIsOpenNuevo = true;
    this.croppedImage = null; 
    this.imageChangedEvent= null; 
    //regEgreso : any = {idEntidadEgreso: 0, idUsuario: 0, fehca : new Date(), monto :0, descripcion:'',evidenciaB64 :''}
    this.regEgreso.descripcion = ''; 
    this.regEgreso.idEntidadEgreso = 0; 
    this.regEgreso.monto = 0; 
    
  }

  entEgresoSelect : any = null; 
  selectEntEgreso(){
    this.lstEntgreso.forEach(element => {
      if(element.idEntidadEgreso == this.regEgreso.idEntidadEgreso ){
        this.entEgresoSelect = element; 
        this.regEgreso.monto = element.montoPagoPeriodico;
        //console.log(this.regEgreso);
        //console.log(this.entEgresoSelect);
      }
    });
    //this.regEgreso.idEntidadEgreso
  }


  insertEgreso(){
    this.regEgreso.idUsuario = this.global.detalle_login.idUsuario; 

    var ahora = new Date();
    this.regEgreso.fehca = new Date(ahora.getFullYear(),ahora.getMonth(),ahora.getDate());
    this.regEgreso.idEntidadEgreso = parseInt( this.regEgreso.idEntidadEgreso.toString() );

    //console.log(this.regEgreso);
    if(this.regEgreso.idEntidadEgreso == 0){ alert('Debe eleguir un destinatario'); return false;}

    this.contabilidad.servInsertarEgreso(this.regEgreso).subscribe(
      (result : any)=>{
        //console.log(result);
        //if(result == false){alert('Ocurrio un problema');}
        this.listarEgresos(1);
        this.mdlIsOpenNuevo = false;
        this.mdlLoad = false;
      }
    );
  }

  listarEntEgreso(){
     this.contabilidad.servListarEntEg().subscribe(
       (result : any)=>{
         //console.log(result);
         this.lstEntgreso = result; 
       }
     );
  }

  
  selectFile(event){
    //console.log(event.target.files.length);
    this.convertBase64(event.target.files[0]);
    
  }

  convertBase64(file: File){
    const observable = new Observable((suscribe:Subscriber<any>)=>{
      this.readFile(file,suscribe);
    });
    observable.subscribe((d)=>{
      //console.log(d);
      this.regEgreso.evidenciaB64 = d;
    },
    (error)=>{alert(error);}
    );
  }

  readFile(file: File ,suscri: Subscriber<any>){
    const fl = new FileReader();
    fl.readAsDataURL(file);

    fl.onload =()=>{
      suscri.next(fl.result);
      suscri.complete();
    }
    fl.onerror=(err)=>{
      suscri.error(err);
      suscri.complete();
    }
  }
}
