import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {AutenticacionService} from '../services/servAutenticacion/autenticacion.service';
import {persona} from '../modelos/persona';
import {detalle_login, postDataLogin} from 'src/app/modelos/detalle_persona';
import { Router } from '@angular/router';
import {GlobalService} from 'src/app/services/global.service';

import * as jspdf  from 'jspdf';
import  html2canvas from 'html2canvas';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-autenticacion',
  templateUrl: './autenticacion.component.html',
  styleUrls: ['./autenticacion.component.css']
})

export class AutenticacionComponent implements OnInit {
  params : postDataLogin ={usuarioSistema:"" , contrasenia :"" } ;   
  data : detalle_login ;
  
    
  constructor( private router : Router, private autenticacion : AutenticacionService, 
    private global: GlobalService, private http : HttpClient) { }

  ngOnInit() {
    sessionStorage.removeItem("usLog");
    sessionStorage.removeItem("menu");
  }


  
  genPDF(){

    var element = document.getElementById('content');
    html2canvas(element).then(
      (canvas)=>{
        console.log(canvas);
        var imgData = canvas.toDataURL('image/png');
        var doc = new jspdf();
        var imgHeight = canvas.height * 208 / canvas.width;

        doc.addImage(imgData,'', 0 , 0 , 208 , imgHeight,'','FAST');
        //var b64 = doc.arrayBufferToBase64; 
        //console.log( b64 );

        console.log( this.enviarMail() );
        doc.save("image.pdf");
      }
    );

  }   

  enviarMail(){
    var param = {email :"mafernandeza_1@outlook.com",
    content :"<html><head><style>table, th, td {border: 1px solid black;background-color: violet;}</style></head><body><table><tr><th>Firstname</th><th>Lastname</th></tr><tr><td>Peter</td><td>Griffin</td></tr><tr><td>Lois</td><td>Griffin</td></tr></table></body>  </html>",
    subject :"xDDD",
    adjunto : "no"}
    

    var  httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',  
          'Authorization':  'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJwZWRyby5tYXJpbiIsImlhdCI6MTYxNjUzMTQyNSwiZXhwIjoxNjE2NTM1MDI1fQ.7YA4qHtBIJqvy0EnxXn2bFHzzC2KDmI92C9BG_dfn4o'
        }) 
      };

    
    return this.http.post('http://localhost/api/email/send' ,param, httpOptions); 
  }
  
  login(){ 
    sessionStorage.removeItem("usLog");
    sessionStorage.removeItem("menu");
    
    this.autenticacion.servicoPOST(this.params ).subscribe( (res:detalle_login)=>{
        this.data = res;
        
        if(this.data.autorizado == true){
          this.global.detalle_login = this.data; 
          sessionStorage.setItem("usLog" , JSON.stringify(this.data));
          
          this.router.navigate(['/inicio'] ); 
          //this.global.detalle_login = this.data;  al dar f5 se  vuelve al estado inicial
          
        }
        else{
          alert('Los datos no estan registrados');
        }
        
      }
    );
  }
}