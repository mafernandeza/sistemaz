import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AutenticacionComponent } from './autenticacion/autenticacion.component';
import { VentasComponent } from './ventas/ventas.component';
import { MenuComponent } from './menu/menu.component';
import { InicioComponent } from './inicio/inicio.component';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';


import { HttpClientModule } from '@angular/common/http';
import { InventarioComponent } from './inventario/inventario.component';
import { MenuGuardGuard } from './services/guards/menu-guard.guard';
import { almacenComponent } from './almacen/almacen.component';
import { PlanillaComponent } from './planilla/planilla.component';
import { ProductoComponent } from './producto/producto.component';
import { ProduccionComponent } from './produccion/produccion.component';
import { UsuarioComponent } from './usuario/usuario.component';
import { ContabilidadComponent } from './contabilidad/contabilidad.component';
import { EntidadEgresoComponent } from './entidad-egreso/entidad-egreso.component';



import { ImageCropperModule } from 'ngx-image-cropper';
import { environment } from '../environments/environment';

import {AngularFireModule} from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireDatabaseModule } from '@angular/fire/database';

const appRoutes: Routes = [
  //{path: '**', component: AutenticacionComponent,},
  {path: '', component: AutenticacionComponent},
  {path: 'autenticacion', component: AutenticacionComponent},
  {path: 'inicio', component: InicioComponent, canActivate:[MenuGuardGuard]},
  {path: 'ventas', component: VentasComponent, canActivate:[MenuGuardGuard]},
  {path: 'inventario', component: InventarioComponent, canActivate:[MenuGuardGuard]},
  {path: 'almacen', component: almacenComponent, canActivate:[MenuGuardGuard]},
  {path: 'planilla', component: PlanillaComponent, canActivate:[MenuGuardGuard]},
  {path: 'producto', component: ProductoComponent, canActivate:[MenuGuardGuard]},
  {path: 'produccion', component: ProduccionComponent, canActivate:[MenuGuardGuard]},
  {path: 'usuario',  component: UsuarioComponent, canActivate : [MenuGuardGuard]},
  {path: 'contabilidad',  component: ContabilidadComponent, canActivate : [MenuGuardGuard]},
  {path: 'entidadEgreso',  component: EntidadEgresoComponent, canActivate : [MenuGuardGuard]}
];


@NgModule({
  declarations: [
    AppComponent,
    AutenticacionComponent,
    VentasComponent,
    MenuComponent,
    InicioComponent,
    InventarioComponent,
    almacenComponent,
    PlanillaComponent,
    ProductoComponent,
    ProduccionComponent,
    UsuarioComponent,
    ContabilidadComponent,
    EntidadEgresoComponent,
    
  ],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireDatabaseModule,
    ImageCropperModule, 
    
    
    FormsModule,
  
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy}], //{provide: LocationStrategy, useClass: HashLocationStrategy}
  bootstrap: [AppComponent]
})
export class AppModule { 
  title = 'sistema z';
}
