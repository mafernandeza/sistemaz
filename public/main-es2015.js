(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/almacen/almacen.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/almacen/almacen.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-menu ></app-menu>\n\n<div class=\"row\" style=\"padding: 1%;\" >\n    \n  <div class=\"col-md-6\">\n\n        <div class=\"panel panel-primary\">\n            <div class=\"panel-heading\">\n              <h3 class=\"panel-title\">Lista Productos</h3>\n            </div>\n            <div class=\"panel-body\">\n                <a class=\"btn\" (click) =\"reiniciarProducto();\" >\n                    Nuevo producto <i class=\"fas fa-plus-circle\"></i>\n                </a>\n                <div class=\"card\">\n                  <strong style=\"padding-top: 8px; padding-left: 8px;\">BUSCAR</strong>\n                  <div class=\"card-body\">\n                    <div class=\"row\">\n                      <div class=\"col-sm-6\">\n                        Nombre: <input type=\"text\" placeholder=\"Todos\" class=\"form-control\" style=\"width:70%; display: inline;\" [(ngModel)]=\"productoBusar.nomProducto\"  (keyup)=\"obtenerProductos(1)\" >\n                      </div>\n                      <div class=\"col-sm-6\">\n                        Categoría: \n                        <select class=\"form-control\" style=\"width:70%; display: inline;\" [(ngModel)]=\"productoBusar.idCategoriaProducto\" (change)=\"obtenerProductos(1)\" >\n                          <option value=\"0\" selected> Todos</option>\n                          <option *ngFor=\"let item of listaCategoriaProducto\" value=\"{{item.idCategoriaProducto}}\">{{item.nombre}}</option>\n                        </select>\n                      </div>\n                    </div>\n                  </div>\n                </div>\n                <hr>\n                <div class=\"table-responsive card contentScroll\">\n                    <table class=\"table tablScroll\" >\n                      <thead>\n                      <tr>\n                        <th style=\"text-align: center;\">Editar</th>\n                        <th>PRODUCTO</th>\n                        <th >STOCK <a class=\"aa\"  [ngClass]=\"{'fa-angle-double-down': productoBusar.ordeby == false, 'fa-angle-double-up': productoBusar.ordeby == true }\" (click)=\"productoBusar.ordeby = !productoBusar.ordeby; obtenerProductos(1)\"></a></th>\n                        <!--<th>PRECIO(compra)</th>-->\n                        <th>Categoria</th>\n                        <th>Costo compra</th>\n                        <th style=\"text-align: center;\">Comprar</th>\n                      </tr>\n                      </thead>\n                      <tbody>\n                      <tr *ngFor=\"let item of listaProducto; let i = index\"  >\n                        <!--<tr *ngFor=\"let item of listaProducto; let i = index\" [ngClass]=\"{'select': filaSelect == i }\" (click)=\"RowSelected(item.idProducto, i, item.nomProducto);\"  > -->\n                        <td style=\"text-align: center;\"> <a (click)=\"editarProducto(item.idProducto, item.nomProducto, item.idCategoriaProducto, item.idUnidadMedida, item.precio, item.idMoneda)\" class=\"fas fa-pen-square\" style=\"cursor:pointer;\"></a></td>\n                        <td>{{item.nomProducto}}</td>\n                        <td>{{item.stock}}</td>\n                        <!--<td>{{item.simboloMoneda}} {{item.precio}}</td>-->\n                        <td>{{item.nombreCategoria}}</td>\n                        <td>S/. {{item.costoCompras}} </td>\n                        <td style=\"text-align: center;\"> <a style=\"cursor:pointer;\" (click)=\"RowSelected(item.idProducto, i, item.nomProducto, item.precioUnidad, item.simboloMoneda, item.idMoneda);\"  class=\"fas fa-shopping-basket\"></a> </td>\n                      </tr>                                           \n                      </tbody>\n                    </table>\n                </div>\n                <div>\n                  <ul class=\"pagination\">\n                    <li class=\"page-item\"><a class=\"page-link aa\" (click)=\"obtenerProductos(1)\"  >Inicio</a></li>\n                    \n                    <li class=\"page-item\" *ngFor=\"let item of pagProducto\" > \n                      <a class=\"page-link aa\" (click)=\"obtenerProductos(item)\"  >{{item}}</a>\n                    </li>\n                    \n                    <li class=\"page-item\"><a class=\"page-link aa\" (click)=\"obtenerProductos(-1)\" >Final</a></li>\n                  </ul>\n                </div>\n            </div>\n        </div>       \n  </div>\n\n  <div class=\"col-md-6\">\n    <div class=\"card padd2\">\n        \n        \n      <button class=\"btn btn-outline-success\"  data-toggle=\"collapse\" data-target=\"#demo0\"> Detalle pedido: {{pedidoActual.NombrePedido}} <i class=\"fas fa-list\"></i></button>\n      <div id=\"demo0\" class=\"collapse show\">\n        <div class=\"alert alert-danger\" [ngStyle]=\"{'display': existePedido == false ? 'block' : 'none' }\">Debe seleccionar un pedido para poder realizar agregar compras (al pedido).</div>\n        <hr>\n        <a id=\"ckNuevoPedido\"  (click)=\"nuevoPedido()\" class=\"fas fa-plus-circle\" style=\"font-size: 12px; cursor: pointer;\" >Nuevo pedido!</a>\n        <div class=\"row\">        \n          <div class=\"col-md-9\">\n            <span>{{nuevoEditantoPedido}}</span>            \n\n            <input type=\"text\" class=\"form-control\" style=\"width: 75%; display: inline;\" [(ngModel)]=\"pedidoActual.NombrePedido\">\n          </div>\n          <div class=\"col-md-3\">             \n            <button class=\"btn btn-success\" style=\"display: inline;\" (click)=\"guardarPedido()\" >Guardar</button>\n          </div>\n\n          <div class=\"col-md-12\">\n            <br>\n            <div style=\"max-height: 200px;\" class=\"table-responsive card\" [ngStyle]=\"{'display': existePedido == false ? 'none' : 'block' }\">\n              <table class=\"table tablScroll\" >\n                <thead>\n                  <th>Producto</th>\n                  <th>Cantidad</th>\n                  <th>Costo</th>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of litaProductosPedito\" >\n                      <td>{{item.nombreProducto}}</td>\n                      <td>{{item.cantidad}}</td>\n                      <td>S/. {{item.precio}}</td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n          </div>\n        \n        </div>        \n      </div>\n\n      <hr>\n      <button class=\"btn btn-outline-success\"  data-toggle=\"collapse\" data-target=\"#demo1\">Historial de pedidos <i class=\"fas fa-clipboard-list\"></i></button>\n\n      <div id=\"demo1\" class=\"collapse show\">\n        <div class=\"table-responsive card\" style=\"max-height: 300px;\">\n          <table class=\"table tablScroll \">\n            <thead>\n            <tr>\n              <th>NOMBRE</th>\n              <th>FECHA</th>\n              <th>Costo Total</th>\n            </tr>\n            </thead>\n            <tbody>\n              <tr *ngFor=\"let item of listaPedido; let i = index\"  (click)=\"obtenerProductosPorPedido(item.idPedido, i, item.nombrePedido);\" [ngClass]=\"{'select': filaSelectPedido == i }\"  >\n                <td>{{item.nombrePedido}}</td>\n                <td>{{item.fechaPedido}}</td>\n                <td>{{item.costoTotal}}</td>\n              </tr>\n            </tbody>\n          </table>\n        </div>      \n      </div>\n    </div>       \n  </div>\n</div>\n\n<!-- The Modal <div class=\"modal \" id=\"ModalNuevoProducto\"  [ngStyle]=\"{'display': mdlIsOpen ? 'block' : 'none'}\">-->\n\n  <div class=\"modal \" id=\"ModalNuevoProducto\" [ngStyle]=\"{'display': mdlIsOpen == true ? 'block' : 'none' }\" >\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n      \n        <!-- Modal Header -->\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Registar nuevo producto</h4>\n          <button type=\"button\" class=\"close\" (click)=\"mdlIsOpen = false\">x</button>\n        </div>\n        \n        <!-- Modal body -->\n        <div class=\"modal-body\">\n            <div class=\"form-group\">\n              <label >Producto:</label>\n              <input type=\"text\" *ngIf=\"product\" [(ngModel)]=\"product.nomProducto\" class=\"form-control\" >\n            </div>\n            <div class=\"form-group\">\n              <label for=\"pwd\">Unidad de medida:</label>\n              <select class=\"form-control\" [(ngModel)]=\"product.IdUnidadMedida\"  >\n                <option *ngFor=\"let item of listaUnidadMedida\" value=\"{{item.idUnidadMedida}}\" >{{item.unidadMedida}}</option>\n              </select>\n            </div>\n            <div class=\"form-group\">\n              <label>Categoria</label>\n              <select class=\"form-control\" [(ngModel)]=\"product.idCategoriaProducto\" >\n                <option *ngFor=\"let item of listaCategoriaProducto\" value=\"{{item.idCategoriaProducto}}\">{{item.nombre}}</option>\n              </select>\n            </div>\n            <div class=\"form-group\">\n              <label>Costo &nbsp; S/. </label>\n              <input type=\"number\" class=\"form-control\" *ngIf=\"product\" [(ngModel)]=\"product.PrecioUnidad\" style=\"width:42%; display: inline;\" >\n              &nbsp;\n              <!--<select class=\"form-control\" style=\"width:42%; display: inline;\" [(ngModel)]=\"product.idMoneda\" >\n                <option *ngFor=\"let item of listaTipoMeneda\" value=\"{{item.idMoneda}}\" >{{item.nombre}} ({{item.simbolo}}) </option>\n              </select>-->\n            </div>\n        </div>\n        \n        <!-- Modal footer -->\n        <div class=\"modal-footer\">\n            <button type=\"button\" (click)=\"insertarProducto()\" class=\"btn btn-success\" >Guardar</button>  \n          <button  type=\"button\" class=\"btn btn-danger\" (click)=\"cerrarModalNProduct()\" >Cancelar</button>\n        </div>\n        \n      </div>\n    </div>\n</div>\n\n<div class=\"modal\" id=modalCompra [ngStyle]=\"{'display': mdlIsOpenCompra == true ? 'block' : 'none' }\" >\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n\n      <!-- Modal Header -->\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Registar compra</h4>\n        <button type=\"button\" class=\"close\" (click)=\"mdlIsOpenCompra = false\">x</button>\n      </div>\n\n      <!--Cuerpo-->\n      <div class=\"modal-body\">\n        <div class=\"form-group\">\n          <p>Producto seleccionado: <strong>{{productCompra.nomProducto}}</strong></p>\n          <!--; precio registrado: <strong>{{productCompra.simboloMoneda}}{{precioProducSelect}}</strong>-->\n        </div>\n        <div class=\"form-group\">\n          <div class=\"row\">\n            <div class=\"col-sm-4\"> \n              <p>Cantidad</p> \n            </div>\n            <div class=\"col-sm-8\"> \n              <input type=\"number\" class=\"form-control\" [(ngModel)]=\"productCompra.stock\"  (keyup)=\"calcularPrecio()\">\n            </div>\n\n            <div class=\"col-sm-4\"><p>Precio unitario</p></div>\n            <div class=\"col-sm-8\">\n              <input type=\"number\" class=\"form-control\" [(ngModel)]=\"productCompra.PrecioUnidad\" (keyup)=\"calcularPrecio()\"  >\n            </div>\n\n            <div class=\"col-sm-4\"><p>Costo total S/.</p></div> \n            <div class=\"col-sm-8\">\n              <input type=\"number\" class=\"form-control\" style=\"width:100%; display: inline;\" [(ngModel)]=\"productCompra.PrecioTotal\">\n              <!--<select class=\"form-control\" style=\"width:40%; display: inline;\" [(ngModel)]=\"productCompra.idMoneda\" disabled>\n                <option *ngFor=\"let item of listaTipoMeneda\" value=\"{{item.idMoneda}}\" >{{item.nombre}} ({{item.simbolo}}) </option>\n              </select>-->\n            </div>\n          </div>\n        </div>\n      </div>\n\n      <!-- Modal footer -->\n      <div class=\"modal-footer\">\n        <button type=\"button\" (click)=\"insertarCompra()\" class=\"btn btn-success\" >Guardar</button>  \n        <button  type=\"button\" class=\"btn btn-danger\" (click)=\"mdlIsOpenCompra = false\" >Cancelar</button>\n      </div>\n    </div>\n  </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\r\n<router-outlet ></router-outlet>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/autenticacion/autenticacion.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/autenticacion/autenticacion.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" <div class=\"container-fluid\">\n    <div class=\"row mh-100vh\">\n        <div class=\"col-10 col-sm-8 col-md-6 col-lg-6 offset-1 offset-sm-2 offset-md-3 offset-lg-0 align-self-center d-lg-flex align-items-lg-center align-self-lg-stretch bg-white p-5 rounded rounded-lg-0 my-5 my-lg-0\" id=\"login-block\">\n            <div class=\"m-auto w-lg-75 w-xl-50\">\n                <h2 class=\"text-info font-weight-light mb-5\"><i class=\"fa fa-diamond\"></i>Grupo Express</h2>\n                \n                <form>\n                    <div class=\"form-group\"><label class=\"text-secondary\">Usuario</label>\n                        <input class=\"form-control\"  [(ngModel)]=\"params.usuarioSistema\" name=\"usuario\"  type=\"text\" required=\"\" pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,15}$\" inputmode=\"email\">\n                    </div> \n                    <div class=\"form-group\"><label class=\"text-secondary\">Contraseña</label>\n                        <input class=\"form-control\" [(ngModel)]=\"params.contrasenia\"  name=\"contrasenia\"  type=\"password\" required=\"\">\n                    </div>\n                    <button class=\"btn btn-info mt-2\" (click)=\"login()\"  >Iniciar sesión</button>\n                </form>\n                <!--<p class=\"mt-3 mb-0\"><a href=\"#\" class=\"text-info small\">Olvido su contraseña?</a></p>-->\n            </div> \n            \n        </div>\n        <div class=\"col-lg-6 d-flex align-items-end\" id=\"bg-block\" style=\"background-image:url(&quot;assets/img/login.jpg&quot;);background-size:cover;background-position:center center;\">\n            <!--<p class=\"ml-auto small text-dark mb-2\"><em>Photo by&nbsp;</em><a routerLink=\"/home\" class=\"text-dark\"><em>Aldain Austria</em></a><br></p>-->\n        </div>\n    </div> \n    <script>\n        $(document).ready(function () { \n    \n            console.log('ssss');\n        });\n    </script>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/contabilidad/contabilidad.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/contabilidad/contabilidad.component.html ***!
  \************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-menu ></app-menu>\n\n\n<div class=\"row \" style=\"margin-right: 0px;\">\n    <div class=\"col-sm-1\">\n        <button type=\"button\" [routerLink]=\"['/entidadEgreso']\" routerLinkActive=\"router-link-active\" class=\"btn btn-success\"> Mantenimiento entidad Egreso</button>\n\n        <hr />\n        <a class=\"btn btn-success\"   (click)=\"NuevoEgreso()\" ><i class=\"fas fa-plus-circle\"></i> Nuevo Egreso</a>\n    </div>\n    <div class=\"col-sm-11\" >\n        <div class=\"row padd1\">\n            \n                <div class=\"col-sm-4\">\n                    <p>Nombres:\n                        <select class=\"form-control\" [(ngModel)]=\"paramBusquedaEgresos.idEntEgre\" >\n                            <option value=\"0\" >Todos</option>\n                            <option *ngFor=\"let item of lstEntgreso\" value=\"{{item.idEntidadEgreso}}\" >{{item.nombres}}, {{item.apPaterno}} {{item.apMaterno}} </option>\n                        </select>\n                    </p>\n                </div>\n                <div class=\"col-sm-4\">\n                    <p>Monto desde: <input type=\"number\" class=\"form-control\" [(ngModel)]=\"paramBusquedaEgresos.montoDesde\" placeholder=\"Todos\"></p>\n                    \n                </div>\n                <div class=\"col-sm-4\">\n                    <p>Monto hasta: <input type=\"number\" class=\"form-control\" [(ngModel)]=\"paramBusquedaEgresos.montoHasta\" placeholder=\"Todos\"></p>\n                    \n                </div>\n                \n                <div class=\"col-sm-4\">\n                    <p>Periodo desde: \n                        <input type=\"date\" class=\"form-control\" [(ngModel)]=\"paramBusquedaEgresos.fDesde\" />\n                    </p>\n                </div>\n                <div class=\"col-sm-4\">\n                        <p>Periodo, hasta: \n                            <input type=\"date\" class=\"form-control\" [(ngModel)]=\"paramBusquedaEgresos.fHasta\" />\n                        </p>                    \n                </div>\n                <div class=\"col-sm-4 padd1\">\n                    \n                        <button (click)=\"listarEgresos(1)\" class=\"btn btn-primary\">Buscar</button>\n                    \n                </div>\n        </div> \n        \n        <strong style=\"padding: 5px;\" >El monto total de los \"items\" mostrados es: {{totalMonto}} </strong>\n        <div class=\"table-responsive card contentScroll\" >            \n            <table class=\"table tablScroll\">\n                <thead>\n                    <tr>\n                        <td>NOMBRES</td>\n                        <td>APELLIDOS</td>\n                        <td>DESSCRIPCION</td>\n                        <td style=\"width: 6%;\">FECHA</td>\n                        <td style=\"width: 10%;\">MONTO</td>\n                        <td style=\"width: 6%;\">EVIDENCIA</td>\n                        <td style=\"width: 6%;\"> NOTIFICAR</td>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let item of lstEgreso; let i = index\" [ngClass]=\"{'select': filaSelect == i }\" >\n                        <td>{{item.nombreEntEgreso}}</td>\n                        <td>{{item.apPaterno}} {{item.apMaterno}}</td>\n                        <td>{{item.descripcion}}</td>\n                        <td>{{item.fecha | date:'dd/MM/yyyy'}}</td>\n                        <td>S/. {{item.monto}}</td>\n                        \n                        <!--<img style=\"width: 30px;height:30px\" *ngIf=\"item.evidencia\" src=\"{{item.evidencia}}\">-->\n                        <td style=\"cursor: pointer; text-align: center;\" (click)=\"verEvidencia(item.idEgreso)\" ><i class=\"fas fa-eye\"></i></td>\n                        <td style=\"cursor: pointer;\" (click)=\"notificarWsp(item.celular, item.monto, item.fecha, item.evidencia  )\" > <i class=\"fab fa-whatsapp\"></i> Notificar </td>\n                    </tr>\n                </tbody>\n            </table>            \n        </div>\n        <div>\n            <ul class=\"pagination\">\n              <li class=\"page-item\"><a class=\"page-link aa\" (click)=\"listarEgresos(1)\"  >Inicio</a></li>\n              \n              <li class=\"page-item\" *ngFor=\"let item of pagEgresos\" > \n                <a class=\"page-link aa\" (click)=\"listarEgresos(item)\"  >{{item}}</a>\n              </li>\n              <li class=\"page-item\"><a class=\"page-link aa\" (click)=\"listarEgresos(-1)\" >Final</a></li>\n            </ul>\n          </div>\n    </div>\n    \n</div>\n\n<div class=\"modal\"  [ngStyle]=\"{'display': mdlIsOpenNuevo == true ? 'block' : 'none' }\" >\n    <div class=\"modal-dialog modal-lg\">\n        <div class=\"modal-content\">\n    \n          <!-- Modal Header -->\n          <div class=\"modal-header\">\n            <h3 class=\"modal-title\">Registar egreso</h3>\n            <button type=\"button\" class=\"close\" (click)=\"mdlIsOpenNuevo = false\">x</button>\n          </div>\n    \n          <!--Cuerpo-->\n          <div class=\"modal-body\">\n            <div class=\"form-group\">\n                <div class=\"row\">\n                        <div class=\"col-sm-6\">\n                            <div class=\"form-group\">\n                                \n                                <p>A quien abonará?  </p>\n                                <select class=\"form-control\" [(ngModel)]=\"regEgreso.idEntidadEgreso\" (change)=\"selectEntEgreso()\" >\n                                    <option value=\"0\" disabled>Elegir una opción</option>\n                                    <option *ngFor=\"let item of lstEntgreso\" value=\"{{item.idEntidadEgreso}}\" >{{item.nombres}}, {{item.apPaterno}} {{item.apMaterno}} - {{item.diaPagoPeriodico}} </option>\n                                </select>\n                            </div>\n                            <div class=\"form-group\">\n                                <p>Comentario</p>\n                                <textarea rows=\"4\" class=\"form-control\" [(ngModel)]=\"regEgreso.descripcion\" ></textarea>\n                            </div>\n                            <div class=\"form-group\">\n                                <p >Monto <span *ngIf=\"entEgresoSelect\" [ngStyle]=\"{'display': entEgresoSelect.diaPagoPeriodico == null?'none': 'block'}\" >Día periodico de pago: {{entEgresoSelect.diaPagoPeriodico}}</span></p>\n                                <input type=\"number\" class=\"form-control\" [(ngModel)]=\"regEgreso.monto\" >\n                            </div>\n                        </div>\n                        <div class=\"col-sm-6\">\n                            \n                            <div class=\"form-group\">\n                                <div class=\"\" style=\"height: 20rem;width:370px;overflow-x :scroll ;overflow-y: scroll;\">\n                                    <image-cropper   [maintainAspectRatio]=\"false\"\n\n                                    [imageChangedEvent]=\"imageChangedEvent\"                        \n                                    (imageCropped)=\"imageCropped($event)\"\n                                    (imageLoaded)=\"imageLoaded()\"\n                                    (cropperReady)=\"cropperReady()\"\n                                    (loadImageFailed)=\"loadImageFailed()\">\n                                    </image-cropper>\n                                    <label class=\"custom-file-upload mr-1\">\n                                        <input type=\"file\" (change)=\"fileChangeEvent($event)\">\n                                        \n                                    </label>\n                                <!--<button (click)=\"sendImage()\" type=\"button\" class=\"btn btn-primary btn-sm mr-1\">Enviar img</button>-->\n                                </div >\n                                \n                            </div>                            \n                        </div>\n                </div>\n            </div>\n          </div>   \n\n          <div class=\"modal-footer\">\n            <button type=\"button\" (click)=\"sendImage_insertarEgreso()\" class=\"btn btn-success\" >Guardar</button>  \n            <button  type=\"button\" class=\"btn btn-danger\" (click)=\"mdlIsOpenNuevo = false\" >Cancelar</button>\n          </div>\n        </div> \n    </div>\n</div>\n\n<div class=\"modal\" [ngStyle]=\"{'display': mdlIsOpenZoomImg == true ? 'block' : 'none' }\"  (click)=\"mdlIsOpenZoomImg = false\" >\n    <div class=\"modal-dialog\" >\n        <div class=\"modal-content\" >\n            <div class=\"modal-body\" style=\"text-align: center;\" >\n                <img *ngIf=\"imgSelect != null\" src=\"{{imgSelect}}\" style=\"width: 100%;\" >\n            </div>\n        </div>\n    </div>\n</div>\n\n<!-- The Modal load-->\n<div class=\"modal\"   [ngStyle]=\"{'display': mdlLoad == true ? 'block' : 'none' }\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\" style=\"background-color: transparent; border: none;\" >\n  \n  \n        <!-- Modal body -->\n        <div class=\"modal-body\" style=\"background-color: transparent; border: none;\">\n          <img src=\"assets/img/gif_3.gif\" width=\"20%\" style=\"padding-top: 50%;\" >\n        </div>  \n  \n      </div>\n    </div>\n  </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/entidad-egreso/entidad-egreso.component.html":
/*!****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/entidad-egreso/entidad-egreso.component.html ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-menu ></app-menu>\n\n\n<div class=\"container\">\n    <div class=\"row\">    \n        <div class=\"col-sm-12\">\n            <a [routerLink]=\"['/contabilidad']\" routerLinkActive=\"router-link-active\"  > <i class=\"far fa-arrow-alt-circle-left\"></i> Contabilidad</a>\n            | <a  (click)=\"nuevo()\" class=\"fas fa-plus-circle\" style=\"font-size: 12px; cursor: pointer;\" > Nuevo</a>\n        </div>\n        <hr />\n        <div class=\"col-sm-12\">\n            <table class=\"table tablScroll\">\n                <thead>\n                    <tr>\n                        <td>NOMBRES</td>\n                        <td>CORREO</td>\n                        <td>CELULAR</td>\n                        <td>Día (PAGO)</td>\n                        <td>MONTO (PAGO)</td>\n                        <td>Ver/Editar</td>\n                    </tr>\n                </thead>\n                <tbody>\n                    <tr *ngFor=\"let item of lstEntEgreso; let i = index\" [ngClass]=\"{'select': filaSelect == i }\" >\n                        <td>{{item.nombres}}, {{item.apPaterno}} {{item.apMaterno}}</td>\n                        <td>{{item.correo}}</td>\n                        <td>{{item.celular}}</td>\n                        <td>{{item.diaPagoPeriodico}}</td>\n                        <td>{{item.montoPagoPeriodico}}</td>\n                        <td style=\"cursor: pointer; text-align: center;\" \n                        (click)=\"editar(item.idEntidadEgreso, item.nombres, item.apPaterno, item. apMaterno, item.correo, \n                        item.celular,item.diaPagoPeriodico, item.montoPagoPeriodico, item.pagoPeriodico)\" >\n                            <i class=\"far fa-edit\"></i>\n                        </td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n        \n    </div>\n</div>\n\n\n<div class=\"modal\" [ngStyle]=\"{'display': mdlpen == true ? 'block' : 'none' }\"   >\n    <div class=\"modal-dialog modal-lg\" >\n        <div class=\"modal-content\" >\n            <!-- Modal Header -->\n          <div class=\"modal-header\">\n            <h3 class=\"modal-title\">Registrar</h3>\n            <button type=\"button\" class=\"close\" (click)=\"mdlpen = false\">x</button>\n          </div>\n\n          <div class=\"modal-body\" >\n            <div class=\"row\">\n                <div class=\"col-sm-6\">\n                    <div class=\"form-group\">\n                        <p>Nombres:</p>\n                        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"entEEditarNuevo.Nombres\">\n                    </div>\n                    <div class=\"form-group\">\n                        <p>Apellidos Materno:</p>\n                        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"entEEditarNuevo.ApMaterno\" >\n                    </div>\n                    <div class=\"form-group\">\n                        <p>Correo:</p>\n                        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"entEEditarNuevo.Correo\" >\n                    </div>\n                </div>\n\n                <div class=\"col-sm-6\">\n                    <div class=\"form-group\">\n                        <p>Apellidos paternos:</p>\n                        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"entEEditarNuevo.ApPaterno\" >\n                    </div>\n                    <div class=\"form-group\">\n                        <p>Celular:</p>\n                        <input type=\"text\" class=\"form-control\" [(ngModel)]=\"entEEditarNuevo.Celular\" >\n                    </div>\n                    <div class=\"form-group\">\n                        <label style=\"cursor: pointer;\" >\n                            <input type=\"checkbox\" (click)=\"activarCamposPagoPeriodico();\" [(ngModel)]=\"entEEditarNuevo.PagoPeriodico\" > Pago periodico?\n                        </label>\n                    </div>\n                    <div class=\"row\"  >\n                        <div class=\"col-sm-6\" [ngStyle]=\"{display: entEEditarNuevo.PagoPeriodico == true ? 'block' : 'none'}\">\n                            <p style=\"display: inline; \">Día </p> \n                            <input type=\"number\" class=\"form-control\" style=\"width: 70%; display: inline; \" [(ngModel)]=\"entEEditarNuevo.DiaPagoPeriodico\" >\n                        </div>\n                        <div class=\"col-sm-6\" [ngStyle]=\"{display: entEEditarNuevo.PagoPeriodico == true ? 'block' : 'none'}\">\n                            <p style=\"display: inline; \">Monto </p>\n                            <input type=\"number\" class=\"form-control\" style=\"width: 70%; display: inline;\" [(ngModel)]=\"entEEditarNuevo.MontoPagoPeriodico\" >\n                        </div>                       \n                    </div>\n                </div>\n            </div>                \n          </div>\n          <div class=\"modal-footer\">\n              <button type=\"button\" (click)=\"guardar()\" class=\"btn btn-success\" >Guardar</button>  \n              <button  type=\"button\" class=\"btn btn-danger\" (click)=\"mdlpen = false\" >Cancelar</button>\n          </div>\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/inicio/inicio.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/inicio/inicio.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-menu></app-menu>\n\n\n<div id=\"homeContainer\"  >\n    <div class=\"bodymodulo\" >\n\n      <div class=\"jumbotron text-center homeContainer\" >\n        <!--<img src=\"assets/img/express.png\" width=\"30%\" >-->\n        <h1>GRUPO EXPRESS</h1>\n\n        \n    </div>\n    <div class=\"container\">\n      <div id=\"demo\" class=\"carousel slide\" data-ride=\"carousel\">\n\n        <!-- Indicators -->\n        <ul class=\"carousel-indicators\">\n          <li data-target=\"#demo\" data-slide-to=\"0\" class=\"active\"></li>\n          <li data-target=\"#demo\" data-slide-to=\"1\"></li>\n          \n        </ul>\n      \n        <!-- The slideshow -->\n        <div class=\"carousel-inner\" style=\"width: 100%;\" >\n          <div class=\"carousel-item active\">\n            <div class=\"row\">\n              <div class=\"col-sm-6\">\n              <h3>MISIÓN</h3>\n              <p style=\"font-size: 20px; text-align: justify;\">\n                Somos uno empresa peruana, orientada a cuidar la calidad  de vida  de nuestros  consumidores, con responsabilidad social empresarial; ofreciendo un servicio de alimentación saludable y oportuna, diferenciado por la calidad de nuestros insumos, nuestro proceso y nuestro servicio orientado a satisfacer los expectativos de nuestros consumidores haciéndolos sentir  como  en cosa.\n              </p>\n              </div>\n              <div class=\"col-sm-6\">\n                <img src=\"assets/img/express.png\" alt=\"Misión\">\n                \n              </div>\n            </div>\n            \n          </div>\n          \n          <div class=\"carousel-item\" style=\"width: 100%;\">\n            <div class=\"row\">\n              <div class=\"col-sm-6\">\n                <h3>VISIÓN</h3>\n                <p style=\"font-size: 20px; text-align: justify;\">\n                  Empresa líder en su rubro a nivel país, diferenciada para cuidar la calidad de vida de sus consumidores tiendo como eje fundamental el desarrollo de buenas  prácticas en manufactura  e higiene alimentaria, garantizando un alto estándar en  la  calidad  de  nuestros Productos  y  servicios que superen las expectativas de nuestros consumidores haciéndolos sentir como en casa.\n                </p>\n              </div>\n              <div class=\"col-sm-6\">\n                <img src=\"assets/img/express1.png\" alt=\"Visión\"> \n              </div>\n            </div>\n          </div>\n        </div>\n      \n        <!-- Left and right controls -->\n        <a class=\"carousel-control-prev\" href=\"#demo\" data-slide=\"prev\">\n          <span class=\"carousel-control-prev-icon\"></span>\n        </a>\n        <a class=\"carousel-control-next\" href=\"#demo\" data-slide=\"next\">\n          <span class=\"carousel-control-next-icon\"></span>\n        </a>\n      \n      </div>\n    </div>\n        \n    </div>\n</div>\n\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/inventario/inventario.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/inventario/inventario.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-menu ></app-menu>\n<div class=\"row bodymodulo\" >\n    \n    <div class=\"col-md-10\">\n            <h3> bienes invetariados: </h3>\n        <div class=\"table-responsive card\">\n            <table class=\"table \">\n                <thead>\n                    <th>#</th>\n                    <th>Nombre</th>\n                    <th>Valor monetario</th>\n                    <th>Tipo</th>                    \n                    <th>Detalle</th>\n                </thead>\n                <tbody>\n                    <tr>\n                        <td>1</td>\n                        <td>Moto</td>\n                        <td>$. 7 000</td>\n                        <td>Mueble</td>\n                        <td><i class=\"far fa-eye\"></i></td>\n                    </tr>\n                    <tr>\n                        <td>2</td>\n                        <td>Oficina</td>\n                        <td>$. 70 000</td>\n                        <td>Inmueble</td>\n                        <td><i class=\"far fa-eye\"></i></td>\n                    </tr>\n                </tbody>\n            </table>\n        </div>\n    </div>\n    <div class=\"col-md-2\">\n        <table>\n            <thead>\n                <th colspan=\"2\">Resumen</th>\n            </thead>\n        </table>\n    </div>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.component.html":
/*!********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.component.html ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("    <nav class=\"navbar navbar-light navbar-expand-md sticky-top navigation-clean-button\" style=\"background-color:#37434d;color:#ffffff;\">\r\n        <div class=\"container-fluid\" >\r\n            <a class=\"navbar-brand\" style=\"color: #ffffff;\"  routerLink=\"/inicio\"  *ngIf=\"listaMenu ==null? false: listaMenu[0].visible\" >\r\n                <i class=\"fa fa-home\" ></i> {{listaMenu == null ? \"\" : listaMenu[0].menu }}\r\n            </a>\r\n            <span>Biemvenida(o) {{dataUsuarioLog.nombres}} {{dataUsuarioLog.apPaterno}} {{dataUsuarioLog.apMaterno}}</span>\r\n            <button class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#navcol-1\">\r\n                <span class=\"sr-only\">Toggle navigation</span>\r\n                <span class=\"navbar-toggler-icon\"></span>\r\n            </button>\r\n            <div\r\n                class=\"collapse navbar-collapse\" id=\"navcol-1\">\r\n                <ul class=\"nav navbar-nav ml-auto\">\r\n\r\n                    <li class=\"nav-item\" role=\"presentation\" *ngIf=\"listaMenu ==null? false: listaMenu[4].visible\">\r\n                        <a class=\"nav-link active\" routerLink=\"/planilla\"  style=\"color:#ffffff;\"  >\r\n                            <i class=\"fas fa-users\"></i> {{listaMenu == null ? \"\" : listaMenu[4].menu }}\r\n                        </a>\r\n                    </li>\r\n\r\n                    <li class=\"nav-item\" role=\"presentation\" *ngIf=\"listaMenu ==null? false: listaMenu[1].visible\">\r\n                        <a class=\"nav-link active\" routerLink=\"/inventario\"  style=\"color:#ffffff;\"  >\r\n                            <i class=\"fas fa-box-open\"></i> {{listaMenu == null ? \"\" : listaMenu[1].menu }}\r\n                        </a>\r\n                    </li>\r\n\r\n                    <li class=\"nav-item\" role=\"presentation\" *ngIf=\"listaMenu ==null? false: listaMenu[3].visible\">\r\n                        <a class=\"nav-link active\"  routerLink=\"/almacen\" style=\"color:#ffffff;\">\r\n                            <i class=\"fab fa-buffer\"></i> {{listaMenu == null ? \"\" : listaMenu[3].menu }}\r\n                        </a>\r\n                    </li>\r\n\r\n                    <li class=\"nav-item\" role=\"presentation\" *ngIf=\"listaMenu ==false\">\r\n                        <!--\"listaMenu ==null? false: listaMenu[2].visible\"-->\r\n                        <a class=\"nav-link active\"  routerLink=\"/ventas\" style=\"color:#ffffff;\">\r\n                            <i class=\"fab fa-accusoft\"></i> {{listaMenu == null ? \"\" : listaMenu[2].menu }}\r\n                        </a>\r\n                    </li>\r\n                    <li class=\"nav-item\" role=\"presentation\" *ngIf=\"listaMenu ==null? false: listaMenu[5].visible\" >\r\n                        <a class=\"nav-link active\"  routerLink=\"/produccion\" style=\"color:#ffffff;\">\r\n                            <i class=\"fas fa-cog\"></i> {{listaMenu == null ? \"\" : listaMenu[5].menu }}\r\n                        </a>\r\n                    </li>\r\n                    <li class=\"nav-item\" role=\"presentation\" *ngIf=\"listaMenu ==null? false: listaMenu[6].visible\" >\r\n                        <a class=\"nav-link active\"  routerLink=\"/usuario\" style=\"color:#ffffff;\">\r\n                            <i class=\"fas fa-user\"></i> {{listaMenu == null ? \"\" : listaMenu[6].menu }}\r\n                        </a>\r\n                    </li>\r\n                    <li class=\"nav-item\" role=\"presentation\" *ngIf=\"listaMenu ==null? false: listaMenu[7].visible\" >\r\n                        <a class=\"nav-link active\"  routerLink=\"/contabilidad\" style=\"color:#ffffff;\">\r\n                            <i class=\"fas fa-money-check-alt\"></i> {{listaMenu == null ? \"\" : listaMenu[7].menu }}\r\n                        </a>\r\n                    </li>\r\n\r\n                    <li class=\"nav-item\" role=\"presentation\">\r\n                        <a class=\"nav-link active\" (click)=\"cerrarSession()\" routerLink=\"/autenticacion\" style=\"color:#ffffff; cursor: pointer;\">\r\n                            <i class=\"fas fa-sign-out-alt\"></i> Cerrar sesión\r\n                        </a>\r\n                    </li>\r\n                </ul>\r\n            </div>\r\n        </div>\r\n    </nav>\r\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/planilla/planilla.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/planilla/planilla.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" <app-menu ></app-menu>\n<div class=\"row bodymodulo \"  st>\n    \n    <div class=\"col-md-10\">\n        <div class=\"panel panel-primary\">\n            <div class=\"table-responsive card\">\n                <div class=\"panel-heading\">\n                    <h3> Lista de colaboradores: </h3>                    \n                </div>   \n                <a class=\"btn\"  data-toggle=\"modal\" data-target=\"#Modal\" style=\"text-align: left;\">\n                    Registrar Nuevo <i class=\"fas fa-plus-circle\"></i>\n                </a>\n                <table class=\"table \">\n                    <thead>\n                        <th>#</th>\n                        <th>Nombre</th>\n                        <th>Apellidos</th>\n                        \n                        <th>Area</th>\n                        <th>Detalle</th>\n                    </thead>\n                    <tbody>\n                        <tr>\n                            <td>1</td>\n                            <td>Nombre1</td>\n                            <td>Apellidos</td>\n                            \n                            <td>RRHH</td>\n                            <td><i data-toggle=\"modal\" data-target=\"#Modal\" style=\"cursor: pointer;\" class=\"far fa-eye\"></i></td>\n                        </tr>\n                        <tr>\n                            <td>2</td>\n                            <td>Nombre2</td>\n                            <td>Apellidos</td>\n                            \n                            <td>Sistemas</td>\n                            <td><i data-toggle=\"modal\" data-target=\"#Modal\" style=\"cursor: pointer;\" class=\"far fa-eye\"></i></td>\n                        </tr>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n    </div>\n    \n    <div class=\"col-md-2\">\n        <div class=\"card\">\n            <table class=\"table\">\n                <thead>\n                    <th colspan=\"2\">RESUMEN</th>\n                </thead>\n                <tbody>\n                    <tr>\n                        <td> Total colaboradores </td>\n                        <td> 65 </td>\n                    </tr>\n                    \n                </tbody>\n            </table>\n        </div>\n    </div>\n</div>\n\n\n\n<!-- The Modal -->\n<div class=\"modal \" id=\"Modal\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n      \n        <!-- Modal Header -->\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Registar /Editar</h4>\n          <button type=\"button\" class=\"close\" data-dismiss=\"modal\">×</button>\n        </div>\n        \n        <!-- Modal body -->\n        <div class=\"modal-body\">\n            <div class=\"form-group\">\n              <span>Fecha contratación </span>\n              <input type=\"text\" class=\"form-control\" />\n              \n            </div>\n            <div class=\"form-group\">\n                <span>Hoja de vida </span>\n                <img width=\"5%\" src=\"../../assets/img/Document-40.png\" style=\"cursor: pointer;\">\n                \n            </div>\n        </div>\n        \n        <!-- Modal footer -->\n        <div class=\"modal-footer\">\n            <button type=\"button\"  class=\"btn btn-success\" >Guardar</button>  \n          <button id=\"btnCerrarModalProducto\" type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Cancelar</button>\n        </div>\n        \n      </div>\n    </div>\n</div>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/produccion/produccion.component.html":
/*!********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/produccion/produccion.component.html ***!
  \********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-menu ></app-menu>\n\n<div class=\"row\" style=\"padding: 1%;\" >\n    <div class=\"col-md-6\">\n        <div class=\"panel panel-primary\">\n            <div class=\"panel-heading\">\n              <h3 class=\"panel-title\">Lista Productos</h3>\n            </div>\n            <div class=\"panel-body\">\n\n              <div class=\"card\">\n                <strong style=\"padding-top: 8px; padding-left: 8px;\">BUSCAR</strong>\n                <div class=\"card-body\">\n                  <div class=\"row\">\n                    <div class=\"col-sm-6\">\n                      Nombre: <input type=\"text\" placeholder=\"Todos\" class=\"form-control\" style=\"width:70%; display: inline;\" [(ngModel)]=\"productoBusar.nomProducto\"  (keyup)=\"obtenerProductos(1)\" >\n                    </div>\n                    <div class=\"col-sm-6\">\n                      Categoría: \n                      <select class=\"form-control\" style=\"width:70%; display: inline;\" [(ngModel)]=\"productoBusar.idCategoriaProducto\" (change)=\"obtenerProductos(1)\" >\n                        <option value=\"0\" selected> Todos</option>\n                        <option *ngFor=\"let item of listaCategoriaProducto\" value=\"{{item.idCategoriaProducto}}\">{{item.nombre}}</option>\n                      </select>\n                    </div>\n                  </div>\n                </div>\n              </div>\n              <hr>\n\n              <div class=\"table-responsive card contentScroll\">\n                <table class=\"table tablScroll\"  >\n                  <thead>\n                  <tr>\n                    <th>PRODUCTO</th>\n                    <th >STOCK <a class=\"aa\"  [ngClass]=\"{'fa-angle-double-down': productoBusar.ordeby == false, 'fa-angle-double-up': productoBusar.ordeby == true }\" (click)=\"productoBusar.ordeby = !productoBusar.ordeby; obtenerProductos(1)\"></a></th>\n                    <th>Categoria</th>                        \n                    <th style=\"text-align: center;\">Pedir</th>\n                  </tr>\n                  </thead>\n                  <tbody>\n                  <tr *ngFor=\"let item of listaProducto; let i = index\" [ngClass]=\"{'select': filaSelectProducto == i }\" >\n\n                    <td>{{item.nomProducto}}</td>\n                    <td>{{item.stock}} {{item.nombreUnidadMedida}}</td>\n                    <!--<td>{{item.simboloMoneda}} {{item.precio}}</td> <i class=\"fas fa-angle-double-up\"></i>-->\n                    <td>{{item.nombreCategoria}}</td>\n                    <td style=\"text-align: center;\"> <a style=\"cursor:pointer;\" (click)=\"regConsumoProducto(item.idProducto, i, item.nomProducto, item.precio, item.simboloMoneda, item.idMoneda, item.stock, item.idUnidadMedida);\"  class=\"fas fa-shopping-basket\"></a> </td>\n                  </tr>\n                  </tbody>\n              </table>            \n              </div>\n              <div>\n                <ul class=\"pagination\">\n                  <li class=\"page-item\"><a class=\"page-link aa\" (click)=\"obtenerProductos(1)\"  >Inicio</a></li>\n                  \n                  <li class=\"page-item\" *ngFor=\"let item of pagProducto\" > \n                    <a class=\"page-link aa\" (click)=\"obtenerProductos(item)\"  >{{item}}</a>\n                  </li>\n                  <li class=\"page-item\"><a class=\"page-link aa\" (click)=\"obtenerProductos(-1)\" >Final</a></li>\n                </ul>\n              </div>\n\n            </div>\n        </div>\n    </div>\n    <div class=\"col-md-6\">\n        <div class=\"card padd2\">\n          <button class=\"btn btn-outline-success\"  data-toggle=\"collapse\" data-target=\"#div1\"> Detalle pedido <i class=\"fas fa-list\"></i></button>\n          <div id=\"div1\" class=\"collapse show\">\n            <div class=\"alert alert-danger\" [ngStyle]=\"{'display': consumoInsert.IdConsumo == 0 ? 'block' : 'none' }\">Debe seleccionar un pedido para poder realizar agregar compras (al pedido).</div>\n            <hr>\n            <a id=\"ckNuevoPedido\"  (click)=\"nuevoPedido()\" class=\"fas fa-plus-circle\" style=\"font-size: 12px; cursor: pointer;\" >Nuevo pedido!</a>\n                <div class=\"row\">                  \n                  <div class=\"col-md-9\">\n                     <span>  {{mensajePedido}} </span> <input type=\"text\" class=\"form-control inLine\" [(ngModel)]=\"consumoInsert.Nombre\">\n                  </div>\n                  <div class=\"col-md-3\">\n                    <button class=\"btn btn-success\" (click)=\"insertarConsumo()\" >Guardar</button>\n                  </div>\n                  \n                  <div class=\"col-md-12\">\n                    <br>\n                    <div style=\"max-height: 200px;\" class=\"table-responsive card\" [ngStyle]=\"{'display': consumoInsert.IdConsumo == 0 ? 'none' : 'block' }\">\n                      <table class=\"table tablScroll\" >\n                        <thead>\n                          <th>Producto</th>\n                          <th>Cantidad</th>\n                          <th>Precio</th>\n                        </thead>\n                        <tbody>\n                          <tr *ngFor=\"let item of listaDetalleConsumo\" >\n                              <td>{{item.producto}}</td>\n                              <td>{{item.cantidad}}</td>\n                              <td>{{item.costoTotal}}</td>\n                          </tr>\n                        </tbody>\n                      </table>\n                    </div>\n                  </div>\n                </div>\n          </div>\n        \n          <hr>\n        <button class=\"btn btn-outline-success\"  data-toggle=\"collapse\" data-target=\"#div2\"> Historial pedidos <i class=\"fas fa-clipboard-list\"></i></button>\n          <div id=\"div2\" class=\"collpse show\">\n            \n\n            <div class=\"table-responsive card\" style=\"max-height: 300px;\">\n              <table class=\"table tablScroll \">\n                <thead>\n                <tr>\n                  <th>NOMBRE</th>\n                  <th>FECHA</th>\n                  <th>COSTO</th>\n                </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let item of listaConsumo; let i = index\"  (click)=\"obtenerDetallePorPedido(item.idConsumo, i, item.nombre);\" [ngClass]=\"{'select': filaSelectConsumo == i }\"  >\n                    <td>{{item.nombre}}</td>\n                    <td>{{item.fecha}}</td>\n                    <td>{{item.costoTotal}}</td>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n\n\n          </div>\n      </div>\n    </div>\n  </div>\n\n  <div class=\"modal \" [ngStyle]=\"{'display': mdlPedir == true ? 'block' : 'none' }\">\n    <div class=\"modal-dialog\">\n      <div class=\"modal-content\">\n\n        <!-- Modal Header -->\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\">Registar consumo</h4>\n          <button type=\"button\" class=\"close\" (click)=\"mdlPedir = false\">x</button>\n        </div>\n\n        <div class=\"modal-body\">\n          <p>Producto: <strong>{{productoSelect}}</strong>, Cantidad disponible: <strong>{{cantidadProdutoSelect}}</strong></p>\n          \n            <label>Cantidad</label>\n            <input class=\"form-control\" type=\"number\" (keyup)=\"txtPedir()\" [(ngModel)]=\"insertarDetalleConsumo.cantidad\" >\n            <select class=\"form-control\" disabled [(ngModel)]=\"idUnidadMedidaSelect\" >\n              <option *ngFor=\"let item of listaUnidadMedida\" value=\"{{item.idUnidadMedida}}\" >{{item.unidadMedida}}</option>\n            </select>\n\n        </div>\n        <!-- Modal footer -->\n        <div class=\"modal-footer\">\n            <button type=\"button\" (click)=\"guardarDetalleConsumo()\" class=\"btn btn-success\" >Guardar</button>  \n          <button  type=\"button\" class=\"btn btn-danger\" (click)=\"mdlPedir = false\" >Cancelar</button>\n        </div>\n      </div>\n    </div>\n  </div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/producto/producto.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/producto/producto.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-menu ></app-menu>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario.component.html ***!
  \**************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-menu ></app-menu>\n\n<ul class=\"nav nav-tabs\" id=\"myTab\" role=\"tablist\">\n    <li class=\"nav-item\">\n      <a class=\"nav-link active\" id=\"home-tab\" data-toggle=\"tab\" href=\"#home\" role=\"tab\" aria-controls=\"home\" aria-selected=\"true\">Usuario</a>\n    </li>\n    <li class=\"nav-item\">\n      <a class=\"nav-link\" id=\"profile-tab\" data-toggle=\"tab\" href=\"#profile\" role=\"tab\" aria-controls=\"profile\" aria-selected=\"false\">Perfil</a>\n    </li>\n    \n</ul>\n\n<div class=\"tab-content\" id=\"myTabContent\">\n    <div class=\"tab-pane fade show active\" id=\"home\" role=\"tabpanel\" aria-labelledby=\"home-tab\">\n        \n        <div class=\"col-md-12\">\n           <a class=\"fas fa-plus-circle\" style=\"cursor: pointer; font-size: 20px; padding: 13px;\" (click)=\"nuevoUsuario()\" >Nuevo usuario</a> \n            <br>\n            <div class=\"table-responsive card\">\n                <table class=\"table\">\n                    <thead>\n                        <th>Nombres</th>\n                        <th>Correo</th>\n                        <th>Usuario</th>\n                        <th>Perfil</th>\n                        <th>Sede</th>\n                        <th style=\"text-align: center;\">Editar</th>\n                    </thead>\n                    <tbody>\n                        <tr *ngFor=\"let item of lstUsuarios; let i = index\" [ngClass]=\"{'select': filaSelect == i }\" >\n                            <td>{{item.nombres}}, {{item.apPaterno}} {{item.apMaterno}}</td>\n                            <td>{{item.correo}}</td>\n                            <td>{{item.usuarioSistema}}</td>\n                            <td>{{item.nPerfil}}</td>\n                            <td>{{item.nSede}}</td>\n                            <td style=\"cursor: pointer; text-align: center;\" (click)=\"editarUsuario(item.idUsuario,item.nombres, item.apPaterno, item.apMaterno, item.correo, item.idPerfil, item.idSede, item.usuarioSistema)\" > <i class=\"fas fa-pen-square\"></i> </td>\n                        </tr>\n                    </tbody>\n                </table>\n            </div>\n        </div>\n        \n    </div>\n\n\n    <div class=\"tab-pane fade\" id=\"profile\" role=\"tabpanel\" aria-labelledby=\"profile-tab\">\n        - mantenimiento de perfiles -\n    </div>\n    \n</div>\n\n\n<div class=\"modal\" [ngStyle]=\"{'display': mdUsuario == true ? 'block' : 'none' }\" >\n    <div class=\"modal-dialog modal-lg\">\n      <div class=\"modal-content \">\n  \n        <!-- Modal Header -->\n        <div class=\"modal-header\">\n          <h4 class=\"modal-title\"> {{msjUsuario}}</h4>\n          <button type=\"button\" class=\"close\" (click)=\"mdUsuario = false\">x</button>\n        </div>\n  \n        <!--Cuerpo-->\n        <div class=\"modal-body\">\n          <div class=\"row\">\n            <div class=\"col-md-6 form-group\">\n                <span>Nombres </span>\n                <input type=\"text\" class=\"form-control txtModalUsuario\"  [(ngModel)]=\"usuarioEditando.Nombres\">\n            </div>\n            \n            <div class=\"col-md-6 form-group\">\n                <span>Apellido paterno </span>\n                <input type=\"text\" class=\"form-control txtModalUsuario\" [(ngModel)]=\"usuarioEditando.ApPaterno\">\n            </div>\n            <div class=\"col-md-6 form-group\">\n                <span>Apellido Materno </span>\n                <input type=\"text\" class=\"form-control txtModalUsuario\" [(ngModel)]=\"usuarioEditando.ApMaterno\" >\n            </div>\n            <div class=\"col-md-6 form-group\">\n                <span>Correo </span>\n                <input type=\"text\" class=\"form-control txtModalUsuario\" [(ngModel)]=\"usuarioEditando.Correo\" >\n            </div>\n\n            \n            <div class=\"col-md-6 form-group\">\n                <span>Perfil </span>\n                <select class=\"form-control txtModalUsuario\" [(ngModel)]=\"usuarioEditando.idPerfil\">\n                    <option *ngFor=\"let item of lstPerfil\" value=\"{{item.idPerfil}}\" >{{item.nombre}}  </option>\n                </select>\n            </div>\n            <div class=\"col-md-6 form-group\">\n                <span>Sede </span>\n                <select class=\"form-control txtModalUsuario\" [(ngModel)]=\"usuarioEditando.idSede\">\n                    <option *ngFor=\"let item of lstSede\" value=\"{{item.idSede}}\" >{{item.nombre}}</option>\n                </select>\n            </div>\n            <div class=\"col-md-6 form-group\">\n                <span>Usuario </span>\n                <input type=\"text\" class=\"form-control txtModalUsuario\" [(ngModel)]=\"usuarioEditando.UsuarioSistema\">\n            </div>\n            <div class=\"col-md-6 form-group\">\n                <span>Contraseña nueva</span>\n                <input type=\"password\" class=\"form-control txtModalUsuario\" [(ngModel)]=\"usuarioEditando.ContraseniaNueva\" >\n            </div>\n            <div class=\"col-md-6\">\n            </div>\n            <div class=\"col-md-6\">\n                <div [ngStyle]=\"{'display': usuarioEditando.idUsuario != 0 ? 'block' : 'none' }\">\n                    <span>Contraseña anterior</span>\n                    <input type=\"password\" class=\"form-control txtModalUsuario\" [(ngModel)]=\"usuarioEditando.ContraseniaAnt\" >\n                </div>\n            </div>\n\n          </div>\n        </div>\n  \n        <!-- Modal footer -->\n        <div class=\"modal-footer\">\n          <button type=\"button\" (click)=\"insertarEditarUsuario()\" class=\"btn btn-success\" >Guardar</button>  \n          <button  type=\"button\" class=\"btn btn-danger\" (click)=\"mdUsuario = false\" >Cancelar</button>\n        </div>\n      </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/ventas/ventas.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/ventas/ventas.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n<app-menu id=\"divMenu1\"  ></app-menu>\n\n\n    <div class=\"row bodymodulo\">\n      <div class=\"col-sm-8\" >\n          <h3>Hacer venta :</h3>\n          \n        <div class=\"card \">\n          <!--<div class=\"card-header\">Header</div>-->\n          <div class=\"card-body\">\n            <strong>Buscar Producto </strong>\n            <div class=\"row\">\n              <div class=\"col-md-8\"> \n                <label>Nombre&nbsp;</label>\n                <input type=\"text\" class=\"form-control\" style=\"display: initial; width: 80%;\" [(ngModel)]=\"params.nombreProducto\" (change)=\"ObtenerProductos()\" />\n              </div>\n              <div class=\"col-md-4\"> \n                <label>Costo maximo&nbsp;</label>\n                <input type=\"number\" class=\"form-control\" style=\"display: initial; width: 60%;\" [(ngModel)]=\"params.costoMaximo\" (change)=\"ObtenerProductos()\" />\n              </div>\n            </div>\n            <br>\n            <table class=\"table\">\n              <thead>\n                <th>PRODUCTO</th>\n                <th>CANTIDAD</th>\n                <th>COSTO UNITARIO</th>\n                <th>AGREGAR</th>\n              </thead>\n              <tbody  >\n                  <tr *ngFor=\"let item of lstProducto; let i = index\" [ngClass]=\"{'select': filaSelect == i }\" (click)=\"RowSelected(item.idProducto, i, item.unidadMedida, item.stock, item.nombre);\">\n                      <td>{{item.nombre}}</td>\n                      <td>{{item.stock}} {{item.unidadMedida}}</td>\n                      <td>{{item.simboloMoneda}} {{item.precio}} por {{item.unidadMedida}}</td>\n                      <td style=\"cursor: pointer;\" data-toggle=\"modal\" data-target=\"#ModalCantidadCompra\"><i class=\"fas fa-cart-plus\" ></i></td>\n                  </tr>\n                  \n              </tbody>\n            </table>\n            \n          </div>\n        </div>\n        \n        <br>\n\n        <div class=\"card\">\n          <div class=\"card-body\">\n            <strong>Detalle de venta:</strong>\n            <table class=\"table\">\n              <thead>\n                  <th>PRODUCTO</th>\n                  <th>CANTIDAD</th>\n              </thead>\n              <tbody id=\"tbDetalleVenta\" ></tbody>\n            </table>\n          </div>\n        </div>  \n        \n      </div>\n\n      <div class=\"col-sm-4\">\n        <h4>Ventas ó ingresos</h4>\n        <div class=\"table-responsive card\">\n            <div class=\"padd2\">\n                <label>Ventas: 17/02/2020 </label>\n            </div>\n            \n\n            <table class=\"table \">\n              <thead>\n                <th>#</th>\n                \n                <th>FECHA</th>\n                <th>Ver Detalle</th>\n              </thead>\n              <tbody>\n                  <tr>\n                      <td>1</td>\n                      \n                      <td>23/02/2019</td>\n                      <td><i class=\"far fa-eye\"></i></td>\n                  </tr>\n                  <tr>\n                    <td>2</td>\n                    \n                    <td>23/02/2019</td>\n                    <td><i class=\"fas fa-eye\"></i></td>\n                </tr>\n                \n                <tr>\n                    <td colspan=\"4\">\n                        <ul class=\"pagination \" >\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Anterior</a></li>\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">1</a></li>\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">2</a></li>\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">3</a></li>\n                            <li class=\"page-item\"><a class=\"page-link\" href=\"#\">Siguiente</a></li>\n                          </ul>\n                    </td>\n                </tr>\n              </tbody>\n            </table>\n            \n      </div>   \n    </div>\n\n    <!-- The Modal -->\n<div class=\"modal \" id=\"ModalCantidadCompra\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n    \n      <!-- Modal Header -->\n      <div class=\"modal-header\">\n        <h4 class=\"modal-title\">Indique la cantidad <span><i class=\"fas fa-cart-plus\" style=\"color: #28a745;\"></i></span></h4>\n      </div>\n      \n      <!-- Modal body -->\n      <div class=\"modal-body\">\n          <div class=\"form-group\">\n            <strong><input type=\"number\" class=\"form-control\" style=\"display:initial; width: 50% ; \" [(ngModel)]=\"cantidadAgregar\" />  {{unidadMedidaSelectt}} </strong>\n          </div>\n      </div>\n      <!-- Modal footer -->\n      <div class=\"modal-footer\">\n          <button type=\"button\"  class=\"btn btn-success\"  (click)=\"Agregar_a_Detalle();\" >Guardar</button>  \n        <button id=\"btnCerrarModalCantidadCompra\" type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Cancelar</button>\n      </div>\n      \n    </div>\n  </div>\n</div>");

/***/ }),

/***/ "./node_modules/tslib/tslib.es6.js":
/*!*****************************************!*\
  !*** ./node_modules/tslib/tslib.es6.js ***!
  \*****************************************/
/*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __createBinding, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault, __classPrivateFieldGet, __classPrivateFieldSet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__extends", function() { return __extends; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__assign", function() { return __assign; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__rest", function() { return __rest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__decorate", function() { return __decorate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__param", function() { return __param; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__metadata", function() { return __metadata; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__awaiter", function() { return __awaiter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__generator", function() { return __generator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__createBinding", function() { return __createBinding; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__exportStar", function() { return __exportStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__values", function() { return __values; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__read", function() { return __read; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spread", function() { return __spread; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__spreadArrays", function() { return __spreadArrays; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__await", function() { return __await; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function() { return __asyncGenerator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function() { return __asyncDelegator; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__asyncValues", function() { return __asyncValues; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function() { return __makeTemplateObject; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importStar", function() { return __importStar; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__importDefault", function() { return __importDefault; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldGet", function() { return __classPrivateFieldGet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "__classPrivateFieldSet", function() { return __classPrivateFieldSet; });
/*! *****************************************************************************
Copyright (c) Microsoft Corporation.

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
PERFORMANCE OF THIS SOFTWARE.
***************************************************************************** */
/* global Reflect, Promise */

var extendStatics = function(d, b) {
    extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return extendStatics(d, b);
};

function __extends(d, b) {
    extendStatics(d, b);
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
}

var __assign = function() {
    __assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }
        return t;
    }
    return __assign.apply(this, arguments);
}

function __rest(s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
}

function __decorate(decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
}

function __param(paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
}

function __metadata(metadataKey, metadataValue) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
}

function __awaiter(thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
}

function __generator(thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
}

function __createBinding(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}

function __exportStar(m, exports) {
    for (var p in m) if (p !== "default" && !exports.hasOwnProperty(p)) exports[p] = m[p];
}

function __values(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
}

function __read(o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
}

function __spread() {
    for (var ar = [], i = 0; i < arguments.length; i++)
        ar = ar.concat(__read(arguments[i]));
    return ar;
}

function __spreadArrays() {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};

function __await(v) {
    return this instanceof __await ? (this.v = v, this) : new __await(v);
}

function __asyncGenerator(thisArg, _arguments, generator) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var g = generator.apply(thisArg, _arguments || []), i, q = [];
    return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i;
    function verb(n) { if (g[n]) i[n] = function (v) { return new Promise(function (a, b) { q.push([n, v, a, b]) > 1 || resume(n, v); }); }; }
    function resume(n, v) { try { step(g[n](v)); } catch (e) { settle(q[0][3], e); } }
    function step(r) { r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r); }
    function fulfill(value) { resume("next", value); }
    function reject(value) { resume("throw", value); }
    function settle(f, v) { if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]); }
}

function __asyncDelegator(o) {
    var i, p;
    return i = {}, verb("next"), verb("throw", function (e) { throw e; }), verb("return"), i[Symbol.iterator] = function () { return this; }, i;
    function verb(n, f) { i[n] = o[n] ? function (v) { return (p = !p) ? { value: __await(o[n](v)), done: n === "return" } : f ? f(v) : v; } : f; }
}

function __asyncValues(o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
}

function __makeTemplateObject(cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};

function __importStar(mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result.default = mod;
    return result;
}

function __importDefault(mod) {
    return (mod && mod.__esModule) ? mod : { default: mod };
}

function __classPrivateFieldGet(receiver, privateMap) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to get private field on non-instance");
    }
    return privateMap.get(receiver);
}

function __classPrivateFieldSet(receiver, privateMap, value) {
    if (!privateMap.has(receiver)) {
        throw new TypeError("attempted to set private field on non-instance");
    }
    privateMap.set(receiver, value);
    return value;
}


/***/ }),

/***/ "./src/app/almacen/almacen.component.css":
/*!***********************************************!*\
  !*** ./src/app/almacen/almacen.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".card.full-card {\r\n    position: fixed ;\r\n    top: 0 ;\r\n    left: 0 ;\r\n    z-index: 99999;\r\n    border-radius: 0;\r\n     \r\n    width: calc(100vw) !important;\r\n    height: 100vh !important;\r\n    text-align: justify;\r\n  }\r\n  .aa{\r\n    font-family: 'Font Awesome 5 Free';\r\n    font-weight: 900;\r\n    cursor: pointer;\r\n}\r\n  .row{\r\n  margin-right: 0px;\r\n  margin-left: 0px;\r\n }\r\n  .alinj{\r\n   text-align: justify;\r\n   width: 40%;\r\n }\r\n  .contentScroll{\r\n   \r\n   max-height: 400px;\r\n }\r\n  .tablScroll{\r\n   overflow-y: scroll;\r\n }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYWxtYWNlbi9hbG1hY2VuLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxnQkFBZ0I7SUFDaEIsT0FBTztJQUNQLFFBQVE7SUFDUixjQUFjO0lBQ2QsZ0JBQWdCOztJQUVoQiw2QkFBNkI7SUFDN0Isd0JBQXdCO0lBQ3hCLG1CQUFtQjtFQUNyQjtFQUNBO0lBQ0Usa0NBQWtDO0lBQ2xDLGdCQUFnQjtJQUNoQixlQUFlO0FBQ25CO0VBQ0M7RUFDQyxpQkFBaUI7RUFDakIsZ0JBQWdCO0NBQ2pCO0VBRUE7R0FDRSxtQkFBbUI7R0FDbkIsVUFBVTtDQUNaO0VBRUE7O0dBRUUsaUJBQWlCO0NBQ25CO0VBRUE7R0FDRSxrQkFBa0I7Q0FDcEIiLCJmaWxlIjoic3JjL2FwcC9hbG1hY2VuL2FsbWFjZW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jYXJkLmZ1bGwtY2FyZCB7XHJcbiAgICBwb3NpdGlvbjogZml4ZWQgO1xyXG4gICAgdG9wOiAwIDtcclxuICAgIGxlZnQ6IDAgO1xyXG4gICAgei1pbmRleDogOTk5OTk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAwO1xyXG4gICAgIFxyXG4gICAgd2lkdGg6IGNhbGMoMTAwdncpICFpbXBvcnRhbnQ7XHJcbiAgICBoZWlnaHQ6IDEwMHZoICFpbXBvcnRhbnQ7XHJcbiAgICB0ZXh0LWFsaWduOiBqdXN0aWZ5O1xyXG4gIH1cclxuICAuYWF7XHJcbiAgICBmb250LWZhbWlseTogJ0ZvbnQgQXdlc29tZSA1IEZyZWUnO1xyXG4gICAgZm9udC13ZWlnaHQ6IDkwMDtcclxuICAgIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG4gLnJvd3tcclxuICBtYXJnaW4tcmlnaHQ6IDBweDtcclxuICBtYXJnaW4tbGVmdDogMHB4O1xyXG4gfSBcclxuXHJcbiAuYWxpbmp7XHJcbiAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgIHdpZHRoOiA0MCU7XHJcbiB9XHJcblxyXG4gLmNvbnRlbnRTY3JvbGx7XHJcbiAgIFxyXG4gICBtYXgtaGVpZ2h0OiA0MDBweDtcclxuIH1cclxuXHJcbiAudGFibFNjcm9sbHtcclxuICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xyXG4gfSJdfQ== */");

/***/ }),

/***/ "./src/app/almacen/almacen.component.ts":
/*!**********************************************!*\
  !*** ./src/app/almacen/almacen.component.ts ***!
  \**********************************************/
/*! exports provided: almacenComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "almacenComponent", function() { return almacenComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/global.service */ "./src/app/services/global.service.ts");
/* harmony import */ var _services_servAlmacen_producto_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/servAlmacen/producto.service */ "./src/app/services/servAlmacen/producto.service.ts");





let almacenComponent = class almacenComponent {
    constructor(router, almacen, global) {
        this.router = router;
        this.almacen = almacen;
        this.global = global;
        this.listaUnidadMedida = null;
        this.listaTipoMeneda = null;
        this.listaProducto = null;
        this.listaCategoriaProducto = null;
        this.listaPedido = null;
        this.mdlIsOpen = false;
        this.mdlIsOpenCompra = false;
        this.existePedido = false;
        this.product = { nomProducto: '', IdUnidadMedida: 1, idCategoriaProducto: 1 };
        this.filaSelect = null;
        this.filaSelectPedido = null;
        this.productCompra = { nomProducto: 'Debe elegir un producto', idMoneda: 1 }; // utilizado para registar una compra de un producto
        this.productoBusar = { nomProducto: '', idCategoriaProducto: 0, ordeby: false, numPag: 1, tamPag: 10 };
        this.pedidoActual = { NombrePedido: '', FechaPedidoDT: new Date(), idSede: 0 };
        this.nuevoEditantoPedido = "Nuevo pedido: ";
        this.litaProductosPedito = null;
    }
    ngOnInit() {
        //general
        this.obtenerListaUnidadMedidas();
        //this.listarTipoMoneda();
        // genera
        this.obtenerCantidadPaginasProducto();
        this.obtenerProductos(1);
        this.obtenerCategoriasProducto();
        this.listarPedidos();
    }
    obtenerCantidadPaginasProducto() {
        this.almacen.listaProductosContar(this.productoBusar).subscribe((r) => {
            var cantidadP = (r / 10);
            cantidadP = Math.trunc(cantidadP);
            if ((r % 10) > 0)
                cantidadP++;
            var temp = [];
            for (var i = 0; i < cantidadP; i++) {
                temp.push(i + 1);
            }
            this.pagProducto = temp;
        });
    }
    cerrarModalNProduct() {
        this.mdlIsOpen = false;
    }
    //general
    obtenerListaUnidadMedidas() {
        this.almacen.listarUnidadMedida().subscribe((res) => {
            this.listaUnidadMedida = res;
            //console.log(this.listaUnidadMedida);
        });
    }
    //general
    listarTipoMoneda() {
        this.almacen.listarTipoMoneda().subscribe((res) => {
            this.listaTipoMeneda = res;
        });
    }
    reiniciarProducto() {
        this.mdlIsOpen = true;
        this.product.nomProducto = '';
        this.product.IdUnidadMedida = 1;
        this.product.PrecioUnidad = 0;
        this.product.PrecioTotal = 0;
        this.product.idMoneda = 1;
        this.product.idCategoriaProducto = 1;
        this.product.idProducto = 0;
    }
    obtenerProductos(pag) {
        this.global.setDetalle_login();
        this.productoBusar.idSede = this.global.detalle_login.idSede;
        this.productoBusar.idCategoriaProducto = parseInt(this.productoBusar.idCategoriaProducto.toString());
        if (pag == -1) {
            this.productoBusar.numPag = this.pagProducto.length;
        }
        else {
            this.productoBusar.numPag = pag;
        }
        //console.log(this.productoBusar);
        this.almacen.listaProductos(this.productoBusar).subscribe((res) => {
            this.listaProducto = res;
            //console.log(this.listaProducto);
        });
    }
    obtenerCategoriasProducto() {
        this.almacen.listaCategoriasProductos().subscribe((res) => {
            this.listaCategoriaProducto = res;
        });
    }
    editarProducto(idProd, nomProd, idCategoriaProd, idUnidadMed, precio, idMoneda) {
        this.mdlIsOpen = true;
        this.product.nomProducto = nomProd;
        this.product.idProducto = idProd;
        this.product.idCategoriaProducto = (idCategoriaProd);
        this.product.IdUnidadMedida = (idUnidadMed);
        this.product.PrecioUnidad = precio;
        this.product.idMoneda = (idMoneda);
        //console.log(this.product);
    }
    insertarProducto() {
        this.product.IdUsuario = this.global.detalle_login.idUsuario;
        //this.product.IdRazonSocial = this.global.detalle_login.idRazonSocial;
        this.product.IdUnidadMedida = parseInt(this.product.IdUnidadMedida.toString());
        this.product.idCategoriaProducto = parseInt(this.product.idCategoriaProducto.toString());
        this.product.idMoneda = 1; //1 --> SOL //parseInt(this.product.idMoneda.toString());
        this.product.PrecioUnidad = 0;
        if (this.product.nomProducto == '') {
            alert('Completar con nombre de producto.');
            return false;
        }
        console.log(this.product);
        this.almacen.insertarProducto(this.product).subscribe((res) => {
            if (res == true) {
                //document.getElementById('btnCerrarModalProducto').click(); 
                this.mdlIsOpen = false;
                this.obtenerCantidadPaginasProducto();
                this.obtenerProductos(this.productoBusar.numPag);
            }
            else {
                alert('Ocurrio un error.');
                //document.getElementById('btnCerrarModalProducto').click();
                this.mdlIsOpen = false;
            }
        });
    }
    RowSelected(idProducto, numFila, nomProducto, precioAnt, simboloMon, idMoneda) {
        if (this.existePedido == false) {
            alert('Primero de guardar un pedido');
            return false;
        }
        this.mdlIsOpenCompra = true;
        this.filaSelect = numFila;
        this.productCompra.nomProducto = nomProducto;
        this.productCompra.idProducto = idProducto;
        this.productCompra.simboloMoneda = simboloMon;
        this.productCompra.idMoneda = idMoneda;
        this.productCompra.PrecioUnidad = precioAnt; // precio --> el anterior registrado
        //console.log(precioAnt );
    }
    calcularPrecio() {
        if (this.productCompra.stock < 0) {
            this.productCompra.stock = 0;
        }
        this.productCompra.PrecioTotal = (this.productCompra.PrecioUnidad * this.productCompra.stock);
    }
    insertarCompra() {
        if (this.productCompra.idProducto == null) {
            alert('Debe elegir un producto primero');
            return false;
        }
        this.productCompra.idMoneda = parseInt(this.productCompra.idMoneda.toString());
        this.productCompra.idPedido = parseInt(this.pedidoActual.idPedido.toString());
        this.productCompra.fechaCompra = new Date();
        this.almacen.insertarCompra(this.productCompra).subscribe((res) => {
            if (res == true) {
                this.productCompra.PrecioUnidad = null;
                this.productCompra.stock = null;
                this.obtenerProductos(this.productoBusar.numPag);
                this.obtenerProductosPorPedido(this.pedidoActual.idPedido, this.filaSelectPedido, this.pedidoActual.NombrePedido);
                this.listarPedidos();
            }
            else {
                alert('Ocurrio un error');
                this.obtenerProductos(this.productoBusar.numPag);
            }
            this.mdlIsOpenCompra = false;
        });
    }
    listarPedidos() {
        this.almacen.listarPedidos().subscribe((res) => {
            this.listaPedido = res;
            //console.log(this.listaPedido);
        });
    }
    guardarPedido() {
        this.existePedido = true; //this.pedidoActual.NombrePedido;
        this.pedidoActual.idSede = this.global.detalle_login.idSede;
        this.nuevoEditantoPedido = "Editando Pedido: ";
        //console.log(this.pedidoActual );
        if (this.pedidoActual.idSede == 0) {
            alert('Sede no identificada');
            return false;
        }
        this.almacen.insertarPedido(this.pedidoActual).subscribe((idPedidoGen) => {
            //console.log(idPedidoGen);
            this.pedidoActual.idPedido = idPedidoGen;
            this.listarPedidos();
            this.filaSelectPedido = 0;
        });
    }
    obtenerProductosPorPedido(idPedido, i, nomPedid) {
        this.filaSelectPedido = i;
        this.nuevoEditantoPedido = "Editando Pedido: ";
        //console.log(idPedido);
        this.existePedido = true;
        //this.nuevoPedidoCk = false;
        this.pedidoActual.NombrePedido = nomPedid;
        this.pedidoActual.idPedido = idPedido;
        this.almacen.listaCompraProductoPorPedido(idPedido).subscribe((lst) => {
            this.litaProductosPedito = lst;
            //console.log(this.litaProductosPedito);
            //this.obtenerProductosPorPedido(this.pedidoActual.idPedido,this.filaSelectPedido,this.pedidoActual.NombrePedido);
        });
    }
    //nuevoPedidoCk : boolean  = true;
    nuevoPedido() {
        this.existePedido = false;
        console.log(this.existePedido);
        //this.filaSelectPedido = -1;
        this.pedidoActual.NombrePedido = '';
        this.pedidoActual.idPedido = 0;
        this.nuevoEditantoPedido = "Nuevo pedido: ";
    }
};
almacenComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_servAlmacen_producto_service__WEBPACK_IMPORTED_MODULE_4__["almacenService"] },
    { type: _services_global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
almacenComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-almacen',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./almacen.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/almacen/almacen.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./almacen.component.css */ "./src/app/almacen/almacen.component.css")).default]
    })
], almacenComponent);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = 'sistemaz';
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _autenticacion_autenticacion_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./autenticacion/autenticacion.component */ "./src/app/autenticacion/autenticacion.component.ts");
/* harmony import */ var _ventas_ventas_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./ventas/ventas.component */ "./src/app/ventas/ventas.component.ts");
/* harmony import */ var _menu_menu_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./menu/menu.component */ "./src/app/menu/menu.component.ts");
/* harmony import */ var _inicio_inicio_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./inicio/inicio.component */ "./src/app/inicio/inicio.component.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _inventario_inventario_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./inventario/inventario.component */ "./src/app/inventario/inventario.component.ts");
/* harmony import */ var _services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/guards/menu-guard.guard */ "./src/app/services/guards/menu-guard.guard.ts");
/* harmony import */ var _almacen_almacen_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./almacen/almacen.component */ "./src/app/almacen/almacen.component.ts");
/* harmony import */ var _planilla_planilla_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./planilla/planilla.component */ "./src/app/planilla/planilla.component.ts");
/* harmony import */ var _producto_producto_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./producto/producto.component */ "./src/app/producto/producto.component.ts");
/* harmony import */ var _produccion_produccion_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./produccion/produccion.component */ "./src/app/produccion/produccion.component.ts");
/* harmony import */ var _usuario_usuario_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./usuario/usuario.component */ "./src/app/usuario/usuario.component.ts");
/* harmony import */ var _contabilidad_contabilidad_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./contabilidad/contabilidad.component */ "./src/app/contabilidad/contabilidad.component.ts");
/* harmony import */ var _entidad_egreso_entidad_egreso_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./entidad-egreso/entidad-egreso.component */ "./src/app/entidad-egreso/entidad-egreso.component.ts");
/* harmony import */ var ngx_image_cropper__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ngx-image-cropper */ "./node_modules/ngx-image-cropper/fesm2015/ngx-image-cropper.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! ../environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _angular_fire__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/fire */ "./node_modules/@angular/fire/fesm2015/angular-fire.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/fesm2015/angular-fire-auth.js");
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/fesm2015/angular-fire-firestore.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/fesm2015/angular-fire-storage.js");
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @angular/fire/database */ "./node_modules/@angular/fire/fesm2015/angular-fire-database.js");




























const appRoutes = [
    //{path: '**', component: AutenticacionComponent,},
    { path: '', component: _autenticacion_autenticacion_component__WEBPACK_IMPORTED_MODULE_6__["AutenticacionComponent"] },
    { path: 'autenticacion', component: _autenticacion_autenticacion_component__WEBPACK_IMPORTED_MODULE_6__["AutenticacionComponent"] },
    { path: 'inicio', component: _inicio_inicio_component__WEBPACK_IMPORTED_MODULE_9__["InicioComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'ventas', component: _ventas_ventas_component__WEBPACK_IMPORTED_MODULE_7__["VentasComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'inventario', component: _inventario_inventario_component__WEBPACK_IMPORTED_MODULE_12__["InventarioComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'almacen', component: _almacen_almacen_component__WEBPACK_IMPORTED_MODULE_14__["almacenComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'planilla', component: _planilla_planilla_component__WEBPACK_IMPORTED_MODULE_15__["PlanillaComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'producto', component: _producto_producto_component__WEBPACK_IMPORTED_MODULE_16__["ProductoComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'produccion', component: _produccion_produccion_component__WEBPACK_IMPORTED_MODULE_17__["ProduccionComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'usuario', component: _usuario_usuario_component__WEBPACK_IMPORTED_MODULE_18__["UsuarioComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'contabilidad', component: _contabilidad_contabilidad_component__WEBPACK_IMPORTED_MODULE_19__["ContabilidadComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] },
    { path: 'entidadEgreso', component: _entidad_egreso_entidad_egreso_component__WEBPACK_IMPORTED_MODULE_20__["EntidadEgresoComponent"], canActivate: [_services_guards_menu_guard_guard__WEBPACK_IMPORTED_MODULE_13__["MenuGuardGuard"]] }
];
let AppModule = class AppModule {
    constructor() {
        this.title = 'sistema z';
    }
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
            _autenticacion_autenticacion_component__WEBPACK_IMPORTED_MODULE_6__["AutenticacionComponent"],
            _ventas_ventas_component__WEBPACK_IMPORTED_MODULE_7__["VentasComponent"],
            _menu_menu_component__WEBPACK_IMPORTED_MODULE_8__["MenuComponent"],
            _inicio_inicio_component__WEBPACK_IMPORTED_MODULE_9__["InicioComponent"],
            _inventario_inventario_component__WEBPACK_IMPORTED_MODULE_12__["InventarioComponent"],
            _almacen_almacen_component__WEBPACK_IMPORTED_MODULE_14__["almacenComponent"],
            _planilla_planilla_component__WEBPACK_IMPORTED_MODULE_15__["PlanillaComponent"],
            _producto_producto_component__WEBPACK_IMPORTED_MODULE_16__["ProductoComponent"],
            _produccion_produccion_component__WEBPACK_IMPORTED_MODULE_17__["ProduccionComponent"],
            _usuario_usuario_component__WEBPACK_IMPORTED_MODULE_18__["UsuarioComponent"],
            _contabilidad_contabilidad_component__WEBPACK_IMPORTED_MODULE_19__["ContabilidadComponent"],
            _entidad_egreso_entidad_egreso_component__WEBPACK_IMPORTED_MODULE_20__["EntidadEgresoComponent"],
        ],
        imports: [
            _angular_fire__WEBPACK_IMPORTED_MODULE_23__["AngularFireModule"].initializeApp(_environments_environment__WEBPACK_IMPORTED_MODULE_22__["environment"].firebaseConfig),
            _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_25__["AngularFirestoreModule"],
            _angular_fire_auth__WEBPACK_IMPORTED_MODULE_24__["AngularFireAuthModule"],
            _angular_fire_storage__WEBPACK_IMPORTED_MODULE_26__["AngularFireStorageModule"],
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_27__["AngularFireDatabaseModule"],
            ngx_image_cropper__WEBPACK_IMPORTED_MODULE_21__["ImageCropperModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"],
            _angular_common_http__WEBPACK_IMPORTED_MODULE_11__["HttpClientModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forRoot(appRoutes),
        ],
        providers: [{ provide: _angular_common__WEBPACK_IMPORTED_MODULE_10__["LocationStrategy"], useClass: _angular_common__WEBPACK_IMPORTED_MODULE_10__["HashLocationStrategy"] }],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/autenticacion/autenticacion.component.css":
/*!***********************************************************!*\
  !*** ./src/app/autenticacion/autenticacion.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mh-100vh{min-height:100vh}\r\n#login-block{box-shadow:0 0 45px 0 rgba(0,0,0,.4);z-index:2}\r\n@media (max-width:991.98px){#login-block{opacity:.95}#bg-block{position:fixed;top:0;left:0;bottom:0;right:0}}\r\n@media (min-width:992px){.w-lg-75{width:75%}.rounded-lg-0{border-radius:0!important}}\r\n@media (min-width:1200px){.w-xl-50{width:50%}}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXV0ZW50aWNhY2lvbi9hdXRlbnRpY2FjaW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsVUFBVSxnQkFBZ0I7QUFDMUIsYUFBb0csb0NBQW9DLENBQUMsU0FBUztBQUFDLDRCQUE0QixhQUFhLFdBQVcsQ0FBQyxVQUFVLGNBQWMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7QUFBQyx5QkFBeUIsU0FBUyxTQUFTLENBQUMsY0FBYyx5QkFBeUIsQ0FBQztBQUFDLDBCQUEwQixTQUFTLFNBQVMsQ0FBQyIsImZpbGUiOiJzcmMvYXBwL2F1dGVudGljYWNpb24vYXV0ZW50aWNhY2lvbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm1oLTEwMHZoe21pbi1oZWlnaHQ6MTAwdmh9XHJcbiNsb2dpbi1ibG9ja3std2Via2l0LWJveC1zaGFkb3c6MCAwIDQ1cHggMCByZ2JhKDAsMCwwLC40KTstbW96LWJveC1zaGFkb3c6MCAwIDQ1cHggMCByZ2JhKDAsMCwwLC40KTtib3gtc2hhZG93OjAgMCA0NXB4IDAgcmdiYSgwLDAsMCwuNCk7ei1pbmRleDoyfUBtZWRpYSAobWF4LXdpZHRoOjk5MS45OHB4KXsjbG9naW4tYmxvY2t7b3BhY2l0eTouOTV9I2JnLWJsb2Nre3Bvc2l0aW9uOmZpeGVkO3RvcDowO2xlZnQ6MDtib3R0b206MDtyaWdodDowfX1AbWVkaWEgKG1pbi13aWR0aDo5OTJweCl7LnctbGctNzV7d2lkdGg6NzUlfS5yb3VuZGVkLWxnLTB7Ym9yZGVyLXJhZGl1czowIWltcG9ydGFudH19QG1lZGlhIChtaW4td2lkdGg6MTIwMHB4KXsudy14bC01MHt3aWR0aDo1MCV9fSJdfQ== */");

/***/ }),

/***/ "./src/app/autenticacion/autenticacion.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/autenticacion/autenticacion.component.ts ***!
  \**********************************************************/
/*! exports provided: AutenticacionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutenticacionComponent", function() { return AutenticacionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_servAutenticacion_autenticacion_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/servAutenticacion/autenticacion.service */ "./src/app/services/servAutenticacion/autenticacion.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_global_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/global.service */ "./src/app/services/global.service.ts");





let AutenticacionComponent = class AutenticacionComponent {
    constructor(router, autenticacion, global) {
        this.router = router;
        this.autenticacion = autenticacion;
        this.global = global;
        this.params = { usuarioSistema: "", contrasenia: "" };
    }
    ngOnInit() {
        sessionStorage.removeItem("usLog");
        sessionStorage.removeItem("menu");
    }
    login() {
        sessionStorage.removeItem("usLog");
        sessionStorage.removeItem("menu");
        this.autenticacion.servicoPOST(this.params).subscribe((res) => {
            this.data = res;
            if (this.data.autorizado == true) {
                this.global.detalle_login = this.data;
                sessionStorage.setItem("usLog", JSON.stringify(this.data));
                this.router.navigate(['/inicio']);
                //this.global.detalle_login = this.data;  al dar f5 se  vuelve al estado inicial
            }
            else {
                alert('Los datos no estan registrados');
            }
        });
    }
};
AutenticacionComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _services_servAutenticacion_autenticacion_service__WEBPACK_IMPORTED_MODULE_2__["AutenticacionService"] },
    { type: src_app_services_global_service__WEBPACK_IMPORTED_MODULE_4__["GlobalService"] }
];
AutenticacionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-autenticacion',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./autenticacion.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/autenticacion/autenticacion.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./autenticacion.component.css */ "./src/app/autenticacion/autenticacion.component.css")).default]
    })
], AutenticacionComponent);



/***/ }),

/***/ "./src/app/contabilidad/contabilidad.component.css":
/*!*********************************************************!*\
  !*** ./src/app/contabilidad/contabilidad.component.css ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (" .contentScroll{   \r\n    max-height: 400px;\r\n  }\r\n\r\n  .tablScroll{\r\n    overflow-y: scroll;\r\n    overflow-x: scroll;\r\n  }\r\n\r\n  /* width */\r\n\r\n  ::-webkit-scrollbar {\r\n  width: 1px;\r\n  height: 1px;\r\n}\r\n\r\n  /* Track \r\n::-webkit-scrollbar-track {\r\n  box-shadow: inset 0 0 1px grey; \r\n  border-radius: 0px;\r\n}*/\r\n\r\n  /* Handle */\r\n\r\n  ::-webkit-scrollbar-thumb {\r\n  background: #148BFF; \r\n  border-radius: 1px;\r\n}\r\n\r\n  .custom-file-upload {\r\n  border: 1px solid #ccc;\r\n  display: inline-block;\r\n  padding: 6px 12px;\r\n  cursor: pointer;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29udGFiaWxpZGFkL2NvbnRhYmlsaWRhZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJDQUFDO0lBQ0csaUJBQWlCO0VBQ25COztFQUVBO0lBQ0Usa0JBQWtCO0lBQ2xCLGtCQUFrQjtFQUNwQjs7RUFFRixVQUFVOztFQUNWO0VBQ0UsVUFBVTtFQUNWLFdBQVc7QUFDYjs7RUFFQTs7OztFQUlFOztFQUNGLFdBQVc7O0VBQ1g7RUFDRSxtQkFBbUI7RUFDbkIsa0JBQWtCO0FBQ3BCOztFQUVBO0VBQ0Usc0JBQXNCO0VBQ3RCLHFCQUFxQjtFQUNyQixpQkFBaUI7RUFDakIsZUFBZTtBQUNqQiIsImZpbGUiOiJzcmMvYXBwL2NvbnRhYmlsaWRhZC9jb250YWJpbGlkYWQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiAuY29udGVudFNjcm9sbHsgICBcclxuICAgIG1heC1oZWlnaHQ6IDQwMHB4O1xyXG4gIH1cclxuXHJcbiAgLnRhYmxTY3JvbGx7XHJcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XHJcbiAgICBvdmVyZmxvdy14OiBzY3JvbGw7XHJcbiAgfVxyXG5cclxuLyogd2lkdGggKi9cclxuOjotd2Via2l0LXNjcm9sbGJhciB7XHJcbiAgd2lkdGg6IDFweDtcclxuICBoZWlnaHQ6IDFweDtcclxufVxyXG5cclxuLyogVHJhY2sgXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gIGJveC1zaGFkb3c6IGluc2V0IDAgMCAxcHggZ3JleTsgXHJcbiAgYm9yZGVyLXJhZGl1czogMHB4O1xyXG59Ki9cclxuLyogSGFuZGxlICovXHJcbjo6LXdlYmtpdC1zY3JvbGxiYXItdGh1bWIge1xyXG4gIGJhY2tncm91bmQ6ICMxNDhCRkY7IFxyXG4gIGJvcmRlci1yYWRpdXM6IDFweDtcclxufVxyXG5cclxuLmN1c3RvbS1maWxlLXVwbG9hZCB7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2NjYztcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgcGFkZGluZzogNnB4IDEycHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4iXX0= */");

/***/ }),

/***/ "./src/app/contabilidad/contabilidad.component.ts":
/*!********************************************************!*\
  !*** ./src/app/contabilidad/contabilidad.component.ts ***!
  \********************************************************/
/*! exports provided: ContabilidadComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContabilidadComponent", function() { return ContabilidadComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_fire_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/storage */ "./node_modules/@angular/fire/fesm2015/angular-fire-storage.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _services_global_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/global.service */ "./src/app/services/global.service.ts");
/* harmony import */ var _services_servContabilidad_contabilidad_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../services/servContabilidad/contabilidad.service */ "./src/app/services/servContabilidad/contabilidad.service.ts");






let ContabilidadComponent = class ContabilidadComponent {
    constructor(contabilidad, global, fbStorage) {
        this.contabilidad = contabilidad;
        this.global = global;
        this.fbStorage = fbStorage;
        this.mdlIsOpenNuevo = false;
        this.mdlIsOpenZoomImg = false;
        this.mdlLoad = false;
        this.imgSelect = 'assets/img/gif_2.gif';
        this.lstEntgreso = null;
        this.lstEgreso = null;
        this.regEgreso = { idEntidadEgreso: 0, idUsuario: 0, fehca: new Date(), monto: 0, descripcion: '', evidenciaB64: '' };
        this.paramBusquedaEgresos = { numPag: 1, tamPag: 10, idEntEgre: 0, montoDesde: null, montoHasta: null, fDesde: null, fHasta: null };
        this.mensajewsp = "";
        this.croppedImage = '';
        this.imageChangedEvent = '';
        this.totalMonto = 0;
        this.entEgresoSelect = null;
    }
    ngOnInit() {
        this.listarEntEgreso();
        this.listarEgresos(1);
    }
    sendImage_insertarEgreso() {
        this.mdlLoad = true;
        if (this.croppedImage != null) {
            const imgID = Date.now();
            const pictur = this.fbStorage.ref('EvidenciaContabilidad/' + imgID + '.jpg').putString(this.croppedImage, 'data_url');
            pictur.then((result) => {
                //co nsole.log(result);
                this.fbStorage.ref('EvidenciaContabilidad/' + imgID + '.jpg').getDownloadURL().subscribe((url_gen) => {
                    //console.log(url_gen);
                    this.regEgreso.evidenciaB64 = url_gen;
                    this.insertEgreso();
                });
            }).catch((error) => { console.log('error enviar imagen', error); });
        }
        else {
            this.insertEgreso();
        }
    }
    fileChangeEvent(event) {
        this.imageChangedEvent = event;
        //console.log(event);
    }
    imageCropped(event) {
        this.croppedImage = event.base64;
    }
    imageLoaded() {
        // show cropper
    }
    cropperReady() {
        // cropper ready
    }
    loadImageFailed() {
        // show message
    }
    notificarWsp(celular, monto, fecha, evidencia) {
        this.mensajewsp = 'https://api.whatsapp.com/send?phone=51' + celular + '&text= Estimado(a), se le notica el depocitado';
        this.mensajewsp += ' de S/. ' + monto + ' en la fecha ' + new Date(fecha).toLocaleString().split(' ')[0];
        this.mensajewsp += "    ; la evidencia puede ver haciendo click en: ";
        window.open(this.mensajewsp, "_blank");
    }
    verEvidencia(val) {
        this.mdlIsOpenZoomImg = true;
        this.imgSelect = 'assets/img/gif_2.gif';
        var param = { idEgreso: val };
        this.contabilidad.servobtenerEvidencia(param).subscribe((result) => {
            this.imgSelect = result;
        });
    }
    listarEgresos(pag) {
        this.paramBusquedaEgresos.numPag = pag;
        this.paramBusquedaEgresos.idEntEgre = parseInt(this.paramBusquedaEgresos.idEntEgre.toString());
        if (pag == -1) {
            this.paramBusquedaEgresos.numPag = this.pagEgresos.length;
        }
        // fechas desde - hasta 
        if (this.paramBusquedaEgresos.fDesde == null || this.paramBusquedaEgresos.fHasta == null) {
            this.paramBusquedaEgresos.fDesde = new Date(1999, 0);
            this.paramBusquedaEgresos.fHasta = new Date(2999, 0);
        }
        //console.log(this.paramBusquedaEgresos);
        this.contabilidad.servListarEgresos(this.paramBusquedaEgresos).subscribe((result) => {
            this.lstEgreso = result;
            //console.log(this.lstEgreso);
            this.listarEgresosContar();
        });
    }
    listarEgresosContar() {
        if (this.paramBusquedaEgresos.fDesde == null || this.paramBusquedaEgresos.fHasta == null) {
            this.paramBusquedaEgresos.fDesde = new Date(1999, 0);
            this.paramBusquedaEgresos.fHasta = new Date(2999, 0);
        }
        this.contabilidad.servListarEgresosContar(this.paramBusquedaEgresos).subscribe((r) => {
            //console.log(r);
            this.totalMonto = (r.totalMonto).toFixed(2);
            var cantidadP = (r.totalRegitros / 10);
            cantidadP = Math.trunc(cantidadP);
            if ((r.totalRegitros % 10) > 0)
                cantidadP++;
            var temp = [];
            for (var i = 0; i < cantidadP; i++) {
                temp.push(i + 1);
            }
            this.pagEgresos = temp;
        });
    }
    keyAnio() {
        this.paramBusquedaEgresos.anio = new Date().getFullYear().toString();
        this.paramBusquedaEgresos.mes = new Date().getMonth().toString();
        //console.log(this.paramBusquedaEgresos.anio.toString().substring(0,4));
        //this.paramBusquedaEgresos.anio = this.paramBusquedaEgresos.anio.toString().substring(0,4);
        //this.paramBusquedaEgresos.mes = this.paramBusquedaEgresos.mes.toString().substring(0,2);
    }
    NuevoEgreso() {
        this.mdlIsOpenNuevo = true;
        this.croppedImage = null;
        this.imageChangedEvent = null;
        //regEgreso : any = {idEntidadEgreso: 0, idUsuario: 0, fehca : new Date(), monto :0, descripcion:'',evidenciaB64 :''}
        this.regEgreso.descripcion = '';
        this.regEgreso.idEntidadEgreso = 0;
        this.regEgreso.monto = 0;
    }
    selectEntEgreso() {
        this.lstEntgreso.forEach(element => {
            if (element.idEntidadEgreso == this.regEgreso.idEntidadEgreso) {
                this.entEgresoSelect = element;
                this.regEgreso.monto = element.montoPagoPeriodico;
                //console.log(this.regEgreso);
                //console.log(this.entEgresoSelect);
            }
        });
        //this.regEgreso.idEntidadEgreso
    }
    insertEgreso() {
        this.regEgreso.idUsuario = this.global.detalle_login.idUsuario;
        var ahora = new Date();
        this.regEgreso.fehca = new Date(ahora.getFullYear(), ahora.getMonth(), ahora.getDate());
        this.regEgreso.idEntidadEgreso = parseInt(this.regEgreso.idEntidadEgreso.toString());
        //console.log(this.regEgreso);
        if (this.regEgreso.idEntidadEgreso == 0) {
            alert('Debe eleguir un destinatario');
            return false;
        }
        this.contabilidad.servInsertarEgreso(this.regEgreso).subscribe((result) => {
            //console.log(result);
            //if(result == false){alert('Ocurrio un problema');}
            this.listarEgresos(1);
            this.mdlIsOpenNuevo = false;
            this.mdlLoad = false;
        });
    }
    listarEntEgreso() {
        this.contabilidad.servListarEntEg().subscribe((result) => {
            //console.log(result);
            this.lstEntgreso = result;
        });
    }
    selectFile(event) {
        //console.log(event.target.files.length);
        this.convertBase64(event.target.files[0]);
    }
    convertBase64(file) {
        const observable = new rxjs__WEBPACK_IMPORTED_MODULE_3__["Observable"]((suscribe) => {
            this.readFile(file, suscribe);
        });
        observable.subscribe((d) => {
            //console.log(d);
            this.regEgreso.evidenciaB64 = d;
        }, (error) => { alert(error); });
    }
    readFile(file, suscri) {
        const fl = new FileReader();
        fl.readAsDataURL(file);
        fl.onload = () => {
            suscri.next(fl.result);
            suscri.complete();
        };
        fl.onerror = (err) => {
            suscri.error(err);
            suscri.complete();
        };
    }
};
ContabilidadComponent.ctorParameters = () => [
    { type: _services_servContabilidad_contabilidad_service__WEBPACK_IMPORTED_MODULE_5__["ContabilidadService"] },
    { type: _services_global_service__WEBPACK_IMPORTED_MODULE_4__["GlobalService"] },
    { type: _angular_fire_storage__WEBPACK_IMPORTED_MODULE_2__["AngularFireStorage"] }
];
ContabilidadComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contabilidad',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./contabilidad.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/contabilidad/contabilidad.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./contabilidad.component.css */ "./src/app/contabilidad/contabilidad.component.css")).default]
    })
], ContabilidadComponent);



/***/ }),

/***/ "./src/app/entidad-egreso/entidad-egreso.component.css":
/*!*************************************************************!*\
  !*** ./src/app/entidad-egreso/entidad-egreso.component.css ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2VudGlkYWQtZWdyZXNvL2VudGlkYWQtZWdyZXNvLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/entidad-egreso/entidad-egreso.component.ts":
/*!************************************************************!*\
  !*** ./src/app/entidad-egreso/entidad-egreso.component.ts ***!
  \************************************************************/
/*! exports provided: EntidadEgresoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntidadEgresoComponent", function() { return EntidadEgresoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_servContabilidad_contabilidad_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/servContabilidad/contabilidad.service */ "./src/app/services/servContabilidad/contabilidad.service.ts");



let EntidadEgresoComponent = class EntidadEgresoComponent {
    constructor(contabilidad) {
        this.contabilidad = contabilidad;
        this.mdlpen = false;
        this.entEEditarNuevo = { IdEntidadEgreso: 0 };
        this.lstEntEgreso = null;
    }
    ngOnInit() {
        this.listarEntidadEgreso();
    }
    nuevo() {
        this.entEEditarNuevo.IdEntidadEgreso = 0;
        this.mdlpen = true;
        this.entEEditarNuevo.Nombres = '';
        this.entEEditarNuevo.ApPaterno = '';
        this.entEEditarNuevo.ApMaterno = '';
        this.entEEditarNuevo.Celular = null;
        this.entEEditarNuevo.Correo = '';
        this.entEEditarNuevo.PagoPeriodico = false;
        this.entEEditarNuevo.DiaPagoPeriodico = null;
        this.entEEditarNuevo.MontoPagoPeriodico = null;
    }
    guardar() {
        console.log(this.entEEditarNuevo);
        this.contabilidad.servInsertarEditarEntEg(this.entEEditarNuevo).subscribe((result) => {
            console.log(result);
            this.listarEntidadEgreso();
            this.mdlpen = false;
        });
    }
    listarEntidadEgreso() {
        this.contabilidad.servListarEntEg().subscribe((result) => {
            this.lstEntEgreso = result;
            //console.log(result);
        });
    }
    activarCamposPagoPeriodico() {
        if (this.entEEditarNuevo.PagoPeriodico == true) {
            this.entEEditarNuevo.PagoPeriodico = false;
            this.entEEditarNuevo.MontoPagoPeriodico = null;
            this.entEEditarNuevo.DiaPagoPeriodico = null;
        }
        else {
            this.entEEditarNuevo.PagoPeriodico = true;
        }
    }
    editar(idEntEg, nombres, apPaterno, apMaterno, correo, celular, diaPagoPeriodico, montoPagoPeriodico, pagoPeriodico) {
        this.entEEditarNuevo.IdEntidadEgreso = idEntEg;
        this.entEEditarNuevo.Nombres = nombres;
        this.entEEditarNuevo.Correo = correo;
        this.entEEditarNuevo.ApPaterno = apPaterno;
        this.entEEditarNuevo.Celular = celular;
        this.entEEditarNuevo.ApMaterno = apMaterno;
        this.entEEditarNuevo.DiaPagoPeriodico = diaPagoPeriodico;
        this.entEEditarNuevo.MontoPagoPeriodico = montoPagoPeriodico;
        this.entEEditarNuevo.PagoPeriodico = pagoPeriodico;
        this.mdlpen = true;
    }
};
EntidadEgresoComponent.ctorParameters = () => [
    { type: _services_servContabilidad_contabilidad_service__WEBPACK_IMPORTED_MODULE_2__["ContabilidadService"] }
];
EntidadEgresoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-entidad-egreso',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./entidad-egreso.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/entidad-egreso/entidad-egreso.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./entidad-egreso.component.css */ "./src/app/entidad-egreso/entidad-egreso.component.css")).default]
    })
], EntidadEgresoComponent);



/***/ }),

/***/ "./src/app/inicio/inicio.component.css":
/*!*********************************************!*\
  !*** ./src/app/inicio/inicio.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".degrad{\r\n    /*\r\n    background-image: url('/assets/img/msnbg.jpg');\r\n    background-size: 100%;    \r\n    background-repeat: no-repeat;\r\n    */\r\n    \r\n    background: rgba(255,255,255,1);\r\n    background: -webkit-gradient(left top, left bottom, color-stop(0%, rgba(255,255,255,1)), color-stop(0%, rgba(160,238,243,1)), color-stop(100%, rgba(255,255,255,1)));\r\n    background: linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(160,238,243,1) 0%, rgba(255,255,255,1) 100%);\r\n    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff', GradientType=0 );   \r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaW5pY2lvL2luaWNpby5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0k7Ozs7S0FJQzs7SUFFRCwrQkFBK0I7SUFFL0Isb0tBQW9LO0lBSXBLLGdIQUFnSDtJQUNoSCxvSEFBb0g7QUFDeEgiLCJmaWxlIjoic3JjL2FwcC9pbmljaW8vaW5pY2lvLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZGVncmFke1xyXG4gICAgLypcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnL2Fzc2V0cy9pbWcvbXNuYmcuanBnJyk7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDEwMCU7ICAgIFxyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgICovXHJcbiAgICBcclxuICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LDI1NSwyNTUsMSk7XHJcbiAgICBiYWNrZ3JvdW5kOiAtbW96LWxpbmVhci1ncmFkaWVudCh0b3AsIHJnYmEoMjU1LDI1NSwyNTUsMSkgMCUsIHJnYmEoMTYwLDIzOCwyNDMsMSkgMCUsIHJnYmEoMjU1LDI1NSwyNTUsMSkgMTAwJSk7XHJcbiAgICBiYWNrZ3JvdW5kOiAtd2Via2l0LWdyYWRpZW50KGxlZnQgdG9wLCBsZWZ0IGJvdHRvbSwgY29sb3Itc3RvcCgwJSwgcmdiYSgyNTUsMjU1LDI1NSwxKSksIGNvbG9yLXN0b3AoMCUsIHJnYmEoMTYwLDIzOCwyNDMsMSkpLCBjb2xvci1zdG9wKDEwMCUsIHJnYmEoMjU1LDI1NSwyNTUsMSkpKTtcclxuICAgIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KHRvcCwgcmdiYSgyNTUsMjU1LDI1NSwxKSAwJSwgcmdiYSgxNjAsMjM4LDI0MywxKSAwJSwgcmdiYSgyNTUsMjU1LDI1NSwxKSAxMDAlKTtcclxuICAgIGJhY2tncm91bmQ6IC1vLWxpbmVhci1ncmFkaWVudCh0b3AsIHJnYmEoMjU1LDI1NSwyNTUsMSkgMCUsIHJnYmEoMTYwLDIzOCwyNDMsMSkgMCUsIHJnYmEoMjU1LDI1NSwyNTUsMSkgMTAwJSk7XHJcbiAgICBiYWNrZ3JvdW5kOiAtbXMtbGluZWFyLWdyYWRpZW50KHRvcCwgcmdiYSgyNTUsMjU1LDI1NSwxKSAwJSwgcmdiYSgxNjAsMjM4LDI0MywxKSAwJSwgcmdiYSgyNTUsMjU1LDI1NSwxKSAxMDAlKTtcclxuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCh0byBib3R0b20sIHJnYmEoMjU1LDI1NSwyNTUsMSkgMCUsIHJnYmEoMTYwLDIzOCwyNDMsMSkgMCUsIHJnYmEoMjU1LDI1NSwyNTUsMSkgMTAwJSk7XHJcbiAgICBmaWx0ZXI6IHByb2dpZDpEWEltYWdlVHJhbnNmb3JtLk1pY3Jvc29mdC5ncmFkaWVudCggc3RhcnRDb2xvcnN0cj0nI2ZmZmZmZicsIGVuZENvbG9yc3RyPScjZmZmZmZmJywgR3JhZGllbnRUeXBlPTAgKTsgICBcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/inicio/inicio.component.ts":
/*!********************************************!*\
  !*** ./src/app/inicio/inicio.component.ts ***!
  \********************************************/
/*! exports provided: InicioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioComponent", function() { return InicioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_global_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/global.service */ "./src/app/services/global.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let InicioComponent = class InicioComponent {
    constructor(global, http) {
        this.global = global;
        this.http = http;
    }
    ngOnInit() {
    }
};
InicioComponent.ctorParameters = () => [
    { type: _services_global_service__WEBPACK_IMPORTED_MODULE_2__["GlobalService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
InicioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-inicio',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./inicio.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/inicio/inicio.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./inicio.component.css */ "./src/app/inicio/inicio.component.css")).default]
    })
], InicioComponent);



/***/ }),

/***/ "./src/app/inventario/inventario.component.css":
/*!*****************************************************!*\
  !*** ./src/app/inventario/inventario.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2ludmVudGFyaW8vaW52ZW50YXJpby5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/inventario/inventario.component.ts":
/*!****************************************************!*\
  !*** ./src/app/inventario/inventario.component.ts ***!
  \****************************************************/
/*! exports provided: InventarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InventarioComponent", function() { return InventarioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let InventarioComponent = class InventarioComponent {
    constructor() { }
    ngOnInit() {
    }
};
InventarioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-inventario',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./inventario.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/inventario/inventario.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./inventario.component.css */ "./src/app/inventario/inventario.component.css")).default]
    })
], InventarioComponent);



/***/ }),

/***/ "./src/app/menu/menu.component.css":
/*!*****************************************!*\
  !*** ./src/app/menu/menu.component.css ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL21lbnUvbWVudS5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/menu/menu.component.ts":
/*!****************************************!*\
  !*** ./src/app/menu/menu.component.ts ***!
  \****************************************/
/*! exports provided: MenuComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuComponent", function() { return MenuComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/global.service */ "./src/app/services/global.service.ts");
/* harmony import */ var src_app_services_servMenu_menu_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/servMenu/menu.service */ "./src/app/services/servMenu/menu.service.ts");





let MenuComponent = class MenuComponent {
    constructor(router, globlal, menu) {
        this.router = router;
        this.globlal = globlal;
        this.menu = menu;
        this.peticionMenu = false;
        this.listaMenu = null;
    }
    ngOnInit() {
        this.globlal.setDetalle_login();
        this.evaluarMenu();
    }
    evaluarMenu() {
        this.dataUsuarioLog = this.globlal.detalle_login;
        if (sessionStorage.getItem("menu") == null) {
            this.menu.obtenerMenu(this.globlal.detalle_login.idPerfil).subscribe((res) => {
                this.listaMenu = res;
                //console.log(this.listaMenu);
                sessionStorage.setItem("menu", JSON.stringify(res));
                //console.log('menú obtenido de servicio'); 
            });
        }
        else {
            //console.log('menú obtenido de session'); 
            this.listaMenu = JSON.parse(sessionStorage.getItem("menu"));
        }
    }
    evaluarMenu_ant() {
        console.log(this.dataUsuarioLog);
        //this.dataUsuarioLog = JSON.parse(sessionStorage.getItem("menuActual")) ;
        if (this.globlal.detalle_login) {
            //console.log('datos de usuario ---> global');
            this.dataUsuarioLog = this.globlal.detalle_login;
        }
        else {
            //console.log('datos de usuario ---> sesion');
            this.dataUsuarioLog = JSON.parse(sessionStorage.getItem("usLog"));
            this.globlal.detalle_login = this.dataUsuarioLog;
        }
        if (sessionStorage.getItem("menu") == null) {
            this.menu.obtenerMenu(this.dataUsuarioLog.idPerfil).subscribe((res) => {
                this.listaMenu = res;
                sessionStorage.setItem("menu", JSON.stringify(res));
                //console.log('menú obtenido de servicio'); 
            });
        }
        else {
            //console.log('menú obtenido de session'); 
            this.listaMenu = JSON.parse(sessionStorage.getItem("menu"));
        }
        console.log(this.dataUsuarioLog);
    }
    cerrarSession() {
        sessionStorage.removeItem("usLog");
        sessionStorage.removeItem("menu");
        this.router.navigate(['']);
    }
};
MenuComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_services_global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] },
    { type: src_app_services_servMenu_menu_service__WEBPACK_IMPORTED_MODULE_4__["MenuService"] }
];
MenuComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-menu',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./menu.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/menu/menu.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./menu.component.css */ "./src/app/menu/menu.component.css")).default]
    })
], MenuComponent);



/***/ }),

/***/ "./src/app/planilla/planilla.component.css":
/*!*************************************************!*\
  !*** ./src/app/planilla/planilla.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BsYW5pbGxhL3BsYW5pbGxhLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/planilla/planilla.component.ts":
/*!************************************************!*\
  !*** ./src/app/planilla/planilla.component.ts ***!
  \************************************************/
/*! exports provided: PlanillaComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlanillaComponent", function() { return PlanillaComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let PlanillaComponent = class PlanillaComponent {
    constructor() { }
    ngOnInit() {
    }
};
PlanillaComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-planilla',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./planilla.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/planilla/planilla.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./planilla.component.css */ "./src/app/planilla/planilla.component.css")).default]
    })
], PlanillaComponent);



/***/ }),

/***/ "./src/app/produccion/produccion.component.css":
/*!*****************************************************!*\
  !*** ./src/app/produccion/produccion.component.css ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".inLine{\r\n    display: inline;\r\n    width: 82%;\r\n}\r\n\r\n.aa{\r\n    font-family: 'Font Awesome 5 Free';\r\n    font-weight: 900;\r\n    cursor: pointer;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvZHVjY2lvbi9wcm9kdWNjaW9uLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxlQUFlO0lBQ2YsVUFBVTtBQUNkOztBQUVBO0lBQ0ksa0NBQWtDO0lBQ2xDLGdCQUFnQjtJQUNoQixlQUFlO0FBQ25CIiwiZmlsZSI6InNyYy9hcHAvcHJvZHVjY2lvbi9wcm9kdWNjaW9uLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW5MaW5le1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgd2lkdGg6IDgyJTtcclxufVxyXG5cclxuLmFhe1xyXG4gICAgZm9udC1mYW1pbHk6ICdGb250IEF3ZXNvbWUgNSBGcmVlJztcclxuICAgIGZvbnQtd2VpZ2h0OiA5MDA7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/produccion/produccion.component.ts":
/*!****************************************************!*\
  !*** ./src/app/produccion/produccion.component.ts ***!
  \****************************************************/
/*! exports provided: ProduccionComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProduccionComponent", function() { return ProduccionComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_global_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/global.service */ "./src/app/services/global.service.ts");
/* harmony import */ var _services_servAlmacen_producto_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/servAlmacen/producto.service */ "./src/app/services/servAlmacen/producto.service.ts");
/* harmony import */ var _services_servProduccion_produccion_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/servProduccion/produccion.service */ "./src/app/services/servProduccion/produccion.service.ts");





let ProduccionComponent = class ProduccionComponent {
    constructor(almacen, produccion, global) {
        this.almacen = almacen;
        this.produccion = produccion;
        this.global = global;
        this.listaProducto = null;
        this.productoBusar = { idCategoriaProducto: 0, ordeby: true, nomProducto: '', tamPag: 10 };
        this.listaCategoriaProducto = null;
        this.listaConsumo = null;
        this.listaUnidadMedida = null;
        this.filaSelectProducto = null;
        this.consumoInsert = { IdConsumo: 0, FechaDT: new Date(), Nombre: '' };
        this.mdlPedir = false;
        this.productoSelect = "";
        this.cantidadProdutoSelect = 0;
        //existePedido : boolean = false;
        this.mensajePedido = 'Nuevo pedido';
        this.insertarDetalleConsumo = { cantidad: 0, idConsumo: -1 };
        this.idUnidadMedidaSelect = -1;
        this.filaSelectConsumo = null;
        this.listaDetalleConsumo = null;
    }
    ngOnInit() {
        this.obtenerCategoriasProducto();
        this.obtenerCantidadPaginasProducto();
        this.obtenerProductos(1);
        this.listarConsumos();
        this.obtenerListaUnidadMedidas();
    }
    obtenerListaUnidadMedidas() {
        this.almacen.listarUnidadMedida().subscribe((res) => {
            this.listaUnidadMedida = res;
            //console.log(this.listaUnidadMedida);
        });
    }
    obtenerCantidadPaginasProducto() {
        this.almacen.listaProductosContar(this.productoBusar).subscribe((r) => {
            var cantidadP = (r / 10);
            cantidadP = Math.trunc(cantidadP);
            if ((r % 10) > 0)
                cantidadP++;
            var temp = [];
            for (var i = 0; i < cantidadP; i++) {
                temp.push(i + 1);
            }
            this.pagProducto = temp;
        });
    }
    obtenerProductos(pag) {
        this.global.setDetalle_login();
        this.productoBusar.idCategoriaProducto = parseInt(this.productoBusar.idCategoriaProducto.toString());
        this.productoBusar.idSede = this.global.detalle_login.idSede;
        if (pag == -1) {
            this.productoBusar.numPag = this.pagProducto.length;
        }
        else {
            this.productoBusar.numPag = pag;
        }
        //console.log(this.productoBusar);
        this.almacen.listaProductos(this.productoBusar).subscribe((res) => {
            this.listaProducto = res;
            //console.log(this.listaProducto);
        });
    }
    obtenerCategoriasProducto() {
        this.almacen.listaCategoriasProductos().subscribe((obj) => {
            this.listaCategoriaProducto = obj;
        });
    }
    listarConsumos() {
        this.produccion.listaConsumo().subscribe((obj) => {
            this.listaConsumo = obj;
            //console.log(this.listaConsumo);
        });
    }
    regConsumoProducto(idProducto, numFila, nomProducto, precio, simboloMon, idMoneda, cantidad, idUnidadMedidaSel) {
        if (this.insertarDetalleConsumo.idConsumo == -1) {
            alert('Primero debe seleccionar un pedido!');
            return false;
        }
        this.filaSelectProducto = numFila;
        this.productoSelect = nomProducto;
        this.cantidadProdutoSelect = cantidad;
        this.mdlPedir = true;
        this.idUnidadMedidaSelect = idUnidadMedidaSel;
        this.insertarDetalleConsumo.idProducto = idProducto;
        //console.log(numFila, idProducto, nomProducto, precio, simboloMon, idMoneda);
    }
    insertarConsumo() {
        this.produccion.insertarConsumo(this.consumoInsert).subscribe((result) => {
            //console.log('idGen: ' + result);
            this.listarConsumos();
            //this.idConsumoActual = result;
            this.consumoInsert.IdConsumo = result;
            this.mensajePedido = "Editar pedido";
        });
    }
    obtenerDetallePorPedido(idConsumo, i, nomConsumo) {
        this.filaSelectConsumo = i;
        this.mensajePedido = "Editar pedido";
        this.consumoInsert.Nombre = nomConsumo;
        this.consumoInsert.IdConsumo = idConsumo;
        this.insertarDetalleConsumo.idConsumo = idConsumo;
        this.produccion.servdetalleConsumo(this.consumoInsert).subscribe((result) => {
            this.listaDetalleConsumo = result;
            //console.log(this.listaDetalleConsumo);
        });
    }
    nuevoPedido() {
        this.mensajePedido = 'Nuevo pedido';
        this.consumoInsert.Nombre = '';
        this.consumoInsert.IdConsumo = 0;
        this.filaSelectConsumo = -1;
    }
    txtPedir() {
        if (this.insertarDetalleConsumo.cantidad < 1) {
            this.insertarDetalleConsumo.cantidad = 1;
            alert('no puedo ingresar numeros negativos');
        }
        if (this.insertarDetalleConsumo.cantidad > this.cantidadProdutoSelect) {
            this.insertarDetalleConsumo.cantidad = this.cantidadProdutoSelect;
            alert('No puede pedir cantidad superior al Stock');
        }
    }
    guardarDetalleConsumo() {
        if (this.insertarDetalleConsumo.cantidad < 1) {
            alert('Debe ingresar un numero mayor a 0');
            return false;
        }
        this.produccion.servinsertarDetalleConsumo(this.insertarDetalleConsumo).subscribe((result) => {
            console.log(result);
            if (result == true) {
                this.obtenerDetallePorPedido(this.consumoInsert.IdConsumo, this.filaSelectConsumo, this.consumoInsert.Nombre);
                this.obtenerProductos(1);
                this.listarConsumos();
                this.mdlPedir = false;
                this.insertarDetalleConsumo.cantidad = 0;
            }
            else {
                alert('Ocurrio un problema :(');
            }
        });
    }
};
ProduccionComponent.ctorParameters = () => [
    { type: _services_servAlmacen_producto_service__WEBPACK_IMPORTED_MODULE_3__["almacenService"] },
    { type: _services_servProduccion_produccion_service__WEBPACK_IMPORTED_MODULE_4__["ProduccionService"] },
    { type: _services_global_service__WEBPACK_IMPORTED_MODULE_2__["GlobalService"] }
];
ProduccionComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-produccion',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./produccion.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/produccion/produccion.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./produccion.component.css */ "./src/app/produccion/produccion.component.css")).default]
    })
], ProduccionComponent);



/***/ }),

/***/ "./src/app/producto/producto.component.css":
/*!*************************************************!*\
  !*** ./src/app/producto/producto.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3Byb2R1Y3RvL3Byb2R1Y3RvLmNvbXBvbmVudC5jc3MifQ== */");

/***/ }),

/***/ "./src/app/producto/producto.component.ts":
/*!************************************************!*\
  !*** ./src/app/producto/producto.component.ts ***!
  \************************************************/
/*! exports provided: ProductoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProductoComponent", function() { return ProductoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProductoComponent = class ProductoComponent {
    constructor() { }
    ngOnInit() {
    }
};
ProductoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-producto',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./producto.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/producto/producto.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./producto.component.css */ "./src/app/producto/producto.component.css")).default]
    })
], ProductoComponent);



/***/ }),

/***/ "./src/app/services/global.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/global.service.ts ***!
  \********************************************/
/*! exports provided: GlobalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GlobalService", function() { return GlobalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");



let GlobalService = class GlobalService {
    constructor(http) {
        this.http = http;
        this.urlServicio = "";
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json'
            })
        };
        this.url_variable = null;
        // 58534  -- 8025
        //this.urlServicio = "https://apiz.azurewebsites.net/api";    
        //this.urlServicio = "http://sistemaz-002-site2.btempurl.com/api";
        //this.urlServicio =  "http://localhost:8025/api";
        //this.urlServicio = "https://738704143bfb.ngrok.io/api";
        this.urlServicio = "http://3.139.235.200/api";
    }
    setDetalle_login() {
        //this.dataUsuarioLog = JSON.parse(sessionStorage.getItem("usLog"));
        //console.log('set uslog');
        this.detalle_login = JSON.parse(sessionStorage.getItem("usLog"));
    }
};
GlobalService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
GlobalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], GlobalService);



/***/ }),

/***/ "./src/app/services/guards/menu-guard.guard.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/guards/menu-guard.guard.ts ***!
  \*****************************************************/
/*! exports provided: MenuGuardGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuGuardGuard", function() { return MenuGuardGuard; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global.service */ "./src/app/services/global.service.ts");




let MenuGuardGuard = class MenuGuardGuard {
    constructor(router, global) {
        this.router = router;
        this.global = global;
    }
    canActivate(next, state) {
        if (!sessionStorage.getItem("usLog")) {
            return false;
        }
        else
            return true;
    }
};
MenuGuardGuard.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
MenuGuardGuard = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], MenuGuardGuard);



/***/ }),

/***/ "./src/app/services/servAlmacen/producto.service.ts":
/*!**********************************************************!*\
  !*** ./src/app/services/servAlmacen/producto.service.ts ***!
  \**********************************************************/
/*! exports provided: almacenService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "almacenService", function() { return almacenService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global.service */ "./src/app/services/global.service.ts");




let almacenService = class almacenService {
    constructor(http, global) {
        this.http = http;
        this.global = global;
    }
    listarUnidadMedida() {
        return this.http.post(this.global.urlServicio + '/Producto/obtenerUnidadMedida', null, this.global.httpOptions);
    }
    listarTipoMoneda() {
        return this.http.post(this.global.urlServicio + '/Producto/listarTipoMoneda', null, this.global.httpOptions);
    }
    listaProductos(prod) {
        return this.http.post(this.global.urlServicio + '/Producto/listarProductos', prod, this.global.httpOptions);
    }
    listaProductosContar(prod) {
        return this.http.post(this.global.urlServicio + '/Producto/listarProductosContar', prod, this.global.httpOptions);
    }
    listaCategoriasProductos() {
        return this.http.post(this.global.urlServicio + '/Producto/listarCategoriasProducto', null, this.global.httpOptions);
    }
    insertarProducto(prod) {
        return this.http.post(this.global.urlServicio + '/Producto/insertarProductos', prod, this.global.httpOptions);
    }
    insertarCompra(prod) {
        return this.http.post(this.global.urlServicio + '/Producto/insertarCompra', prod, this.global.httpOptions);
    }
    listarPedidos() {
        return this.http.post(this.global.urlServicio + '/Producto/listarPedidos', null, this.global.httpOptions);
    }
    insertarPedido(param) {
        return this.http.post(this.global.urlServicio + '/Producto/insertarPedido', param, this.global.httpOptions);
    }
    listaCompraProductoPorPedido(param) {
        return this.http.post(this.global.urlServicio + '/Producto/listaCompraProductoPorPedido', param, this.global.httpOptions);
    }
};
almacenService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
almacenService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], almacenService);



/***/ }),

/***/ "./src/app/services/servAutenticacion/autenticacion.service.ts":
/*!*********************************************************************!*\
  !*** ./src/app/services/servAutenticacion/autenticacion.service.ts ***!
  \*********************************************************************/
/*! exports provided: AutenticacionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AutenticacionService", function() { return AutenticacionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var src_app_services_global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/global.service */ "./src/app/services/global.service.ts");




let AutenticacionService = class AutenticacionService {
    constructor(http, global) {
        this.http = http;
        this.global = global;
        //url: string = 'http://localhost:58534/api/fLogin/autenticacion';
        this.httpOptions = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
                'Content-Type': 'application/json',
                "cache-control": "no-cache",
            })
        };
    }
    servicoPOST(datax) {
        return this.http.post(this.global.urlServicio + '/fLogin/autenticacion', datax, this.httpOptions);
    }
    sessionActiva() {
        //console.log('en servicio autent:' , this.global.detalle_login);
        if (this.global.detalle_login == null) {
            return false;
        }
        else {
            return true;
        }
    }
};
AutenticacionService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: src_app_services_global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
AutenticacionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], AutenticacionService);



/***/ }),

/***/ "./src/app/services/servContabilidad/contabilidad.service.ts":
/*!*******************************************************************!*\
  !*** ./src/app/services/servContabilidad/contabilidad.service.ts ***!
  \*******************************************************************/
/*! exports provided: ContabilidadService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContabilidadService", function() { return ContabilidadService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _global_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../global.service */ "./src/app/services/global.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");




let ContabilidadService = class ContabilidadService {
    constructor(global, http) {
        this.global = global;
        this.http = http;
    }
    servListarEntEg() {
        return this.http.post(this.global.urlServicio + '/contabilidad/listarEntidadEgreso', null, this.global.httpOptions);
    }
    servInsertarEditarEntEg(param) {
        return this.http.post(this.global.urlServicio + '/contabilidad/insertarEditarEntidad', param, this.global.httpOptions);
    }
    servInsertarEgreso(param) {
        return this.http.post(this.global.urlServicio + '/contabilidad/insertarEgreso', param, this.global.httpOptions);
    }
    servListarEgresos(params) {
        return this.http.post(this.global.urlServicio + '/contabilidad/listarEgresos', params, this.global.httpOptions);
    }
    servobtenerEvidencia(params) {
        return this.http.post(this.global.urlServicio + '/contabilidad/obtenerEvidencia', params, this.global.httpOptions);
    }
    servListarEgresosContar(params) {
        return this.http.post(this.global.urlServicio + '/contabilidad/listarEgresosContar', params, this.global.httpOptions);
    }
};
ContabilidadService.ctorParameters = () => [
    { type: _global_service__WEBPACK_IMPORTED_MODULE_2__["GlobalService"] },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"] }
];
ContabilidadService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ContabilidadService);



/***/ }),

/***/ "./src/app/services/servMenu/menu.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/servMenu/menu.service.ts ***!
  \***************************************************/
/*! exports provided: MenuService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuService", function() { return MenuService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global.service */ "./src/app/services/global.service.ts");




let MenuService = class MenuService {
    constructor(http, global) {
        this.http = http;
        this.global = global;
    }
    obtenerMenu(id) {
        var param = {
            idPerfil: id
        };
        return this.http.post(this.global.urlServicio + '/home/menuPorPerfil', param, this.global.httpOptions);
    }
};
MenuService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
MenuService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], MenuService);



/***/ }),

/***/ "./src/app/services/servProduccion/produccion.service.ts":
/*!***************************************************************!*\
  !*** ./src/app/services/servProduccion/produccion.service.ts ***!
  \***************************************************************/
/*! exports provided: ProduccionService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProduccionService", function() { return ProduccionService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global.service */ "./src/app/services/global.service.ts");




let ProduccionService = class ProduccionService {
    constructor(http, global) {
        this.http = http;
        this.global = global;
    }
    listaConsumo() {
        return this.http.post(this.global.urlServicio + '/producto/listaConsumo', null, this.global.httpOptions);
    }
    insertarConsumo(param) {
        return this.http.post(this.global.urlServicio + '/producto/insertarConsumo', param, this.global.httpOptions);
    }
    servdetalleConsumo(param) {
        return this.http.post(this.global.urlServicio + '/producto/detalleConsumo', param, this.global.httpOptions);
    }
    servinsertarDetalleConsumo(param) {
        return this.http.post(this.global.urlServicio + '/producto/insertarDetalleConsumo', param, this.global.httpOptions);
    }
};
ProduccionService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
ProduccionService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], ProduccionService);



/***/ }),

/***/ "./src/app/services/servUsuario/usuario.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/services/servUsuario/usuario.service.ts ***!
  \*********************************************************/
/*! exports provided: UsuarioService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioService", function() { return UsuarioService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global.service */ "./src/app/services/global.service.ts");




let UsuarioService = class UsuarioService {
    constructor(http, global) {
        this.http = http;
        this.global = global;
    }
    servlistaUsuarios() {
        return this.http.post(this.global.urlServicio + '/Usuario/listaUsuarios', null, this.global.httpOptions);
    }
    servListarPerfil() {
        return this.http.post(this.global.urlServicio + '/Usuario/listaPerfil', null, this.global.httpOptions);
    }
    servListarSede() {
        return this.http.post(this.global.urlServicio + '/Usuario/listarSede', null, this.global.httpOptions);
    }
    servInsertarEditarUsuario(param) {
        return this.http.post(this.global.urlServicio + '/Usuario/insertarEditarUsuario', param, this.global.httpOptions);
    }
};
UsuarioService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"] },
    { type: _global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
UsuarioService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], UsuarioService);



/***/ }),

/***/ "./src/app/services/servVenta/venta.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/servVenta/venta.service.ts ***!
  \*****************************************************/
/*! exports provided: VentaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VentaService", function() { return VentaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../global.service */ "./src/app/services/global.service.ts");




let VentaService = class VentaService {
    constructor(http, global) {
        this.http = http;
        this.global = global;
    }
    listaProductos(params) {
        return this.http.post(this.global.urlServicio + '/Venta/obtenerListaProductos', params, this.global.httpOptions);
    }
};
VentaService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
VentaService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], VentaService);



/***/ }),

/***/ "./src/app/usuario/usuario.component.css":
/*!***********************************************!*\
  !*** ./src/app/usuario/usuario.component.css ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".txtModalUsuario{\r\n    display: inline; \r\n    \r\n    \r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXN1YXJpby91c3VhcmlvLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxlQUFlOzs7QUFHbkIiLCJmaWxlIjoic3JjL2FwcC91c3VhcmlvL3VzdWFyaW8uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi50eHRNb2RhbFVzdWFyaW97XHJcbiAgICBkaXNwbGF5OiBpbmxpbmU7IFxyXG4gICAgXHJcbiAgICBcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/usuario/usuario.component.ts":
/*!**********************************************!*\
  !*** ./src/app/usuario/usuario.component.ts ***!
  \**********************************************/
/*! exports provided: UsuarioComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsuarioComponent", function() { return UsuarioComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_global_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/global.service */ "./src/app/services/global.service.ts");
/* harmony import */ var _services_servUsuario_usuario_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/servUsuario/usuario.service */ "./src/app/services/servUsuario/usuario.service.ts");




let UsuarioComponent = class UsuarioComponent {
    constructor(usuario, global) {
        this.usuario = usuario;
        this.global = global;
        this.lstUsuarios = null;
        this.lstPerfil = null;
        this.lstSede = null;
        this.mdUsuario = false;
        this.msjUsuario = 'Nuevo usuario';
        this.usuarioEditando = {};
    }
    ngOnInit() {
        this.listaUsuarios();
        this.listarPerfiles();
        this.listarSede();
    }
    listarSede() {
        this.usuario.servListarSede().subscribe((result) => {
            this.lstSede = result;
            console.log(this.lstSede);
        });
    }
    listarPerfiles() {
        this.usuario.servListarPerfil().subscribe((result) => {
            this.lstPerfil = result;
            //console.log(this.lstPerfil);
        });
    }
    listaUsuarios() {
        this.usuario.servlistaUsuarios().subscribe((result) => {
            this.lstUsuarios = result;
            //console.log(this.lstUsuarios);
        });
    }
    editarUsuario(idus, nomb, apPat, apMat, correo, idPerf, idSed, us) {
        this.mdUsuario = true;
        this.msjUsuario = 'Editar usuario';
        this.usuarioEditando.Nombres = nomb;
        this.usuarioEditando.ApPaterno = apPat;
        this.usuarioEditando.ApMaterno = apMat;
        this.usuarioEditando.Correo = correo;
        this.usuarioEditando.idPerfil = idPerf;
        this.usuarioEditando.idSede = idSed;
        this.usuarioEditando.UsuarioSistema = us;
        this.usuarioEditando.ContraseniaNueva = '';
        this.usuarioEditando.idUsuario = idus;
        this.usuarioEditando.ContraseniaAnt = '';
        this.usuarioEditando.ContraseniaNueva = '';
        //console.log(nomb, this.usuarioEditando.Nombres);
    }
    nuevoUsuario() {
        this.mdUsuario = true;
        this.msjUsuario = 'Nuevo usuario';
        this.usuarioEditando = { idUsuario: 0, ContraseniaNueva: '', ContraseniaAnt: '' };
    }
    insertarEditarUsuario() {
        //console.log(this.usuarioEditando);
        this.usuarioEditando.idSede = parseInt(this.usuarioEditando.idSede.toString());
        this.global.detalle_login.idSede = parseInt(this.usuarioEditando.idSede.toString());
        sessionStorage.setItem("usLog", JSON.stringify(this.global.detalle_login));
        this.usuarioEditando.idPerfil = parseInt(this.usuarioEditando.idPerfil.toString());
        this.usuario.servInsertarEditarUsuario(this.usuarioEditando).subscribe((result) => {
            if (result == true) {
                this.mdUsuario = false;
            }
            else {
                this.mdUsuario = false;
                alert('La contraseña no se ha modificado!');
            }
            this.listaUsuarios();
        });
    }
};
UsuarioComponent.ctorParameters = () => [
    { type: _services_servUsuario_usuario_service__WEBPACK_IMPORTED_MODULE_3__["UsuarioService"] },
    { type: _services_global_service__WEBPACK_IMPORTED_MODULE_2__["GlobalService"] }
];
UsuarioComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-usuario',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./usuario.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/usuario/usuario.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./usuario.component.css */ "./src/app/usuario/usuario.component.css")).default]
    })
], UsuarioComponent);



/***/ }),

/***/ "./src/app/ventas/ventas.component.css":
/*!*********************************************!*\
  !*** ./src/app/ventas/ventas.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3ZlbnRhcy92ZW50YXMuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/ventas/ventas.component.ts":
/*!********************************************!*\
  !*** ./src/app/ventas/ventas.component.ts ***!
  \********************************************/
/*! exports provided: VentasComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VentasComponent", function() { return VentasComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_servVenta_venta_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/servVenta/venta.service */ "./src/app/services/servVenta/venta.service.ts");
/* harmony import */ var _services_global_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/global.service */ "./src/app/services/global.service.ts");




let VentasComponent = class VentasComponent {
    constructor(sVenta, global) {
        this.sVenta = sVenta;
        this.global = global;
        this.params = { nPag: 0, tPag: 10, costoMaximo: 100, idSede: -1, nombreProducto: "" };
        this.lstProducto = null;
        this.filaSelect = null;
        this.unidadMedidaSelectt = "";
        this.stockSelect = 0;
        this.productoSelect = "";
        this.cantidadAgregar = 0;
        this.idProductoSelect = 0;
    }
    ngOnInit() {
        this.global.setDetalle_login();
        this.ObtenerProductos();
    }
    ObtenerProductos() {
        //this.params.idRazonSocial = this.global.detalle_login.idRazonSocial; 
        //console.log(this.params);
        this.sVenta.listaProductos(this.params).subscribe((res) => {
            this.lstProducto = res;
        });
    }
    Agregar_a_Detalle() {
        if (this.cantidadAgregar <= this.stockSelect && this.cantidadAgregar > 0) {
            //console.log('agregar idProducto: '+ this.idProductoSelect+'cantidad: '+this.cantidadAgregar);
            this.ocultarModal('btnCerrarModalCantidadCompra');
            var html = document.getElementById('tbDetalleVenta').innerHTML;
            html += '<tr>' + '<td hidden>' + this.idProductoSelect + '</td><td>' + this.productoSelect + '</td><td>' + this.cantidadAgregar + '</td>' + '</tr>';
            var tb = document.getElementById('tbDetalleVenta');
            tb.innerHTML = html;
            this.cantidadAgregar = 0;
        }
        else {
            //alert('La cantidad excede al Stock disponible actualmente.');
            alert('Cantidad no valida.');
        }
        //console.log(this.cantidadAgregar);
    }
    RowSelected(idProducto, numFila, unidadMedida, stock, nombre) {
        this.idProductoSelect = idProducto;
        this.filaSelect = numFila;
        this.unidadMedidaSelectt = unidadMedida;
        this.stockSelect = stock;
        this.productoSelect = nombre;
    }
    ocultarModal(NombreModal) {
        //$('#ModalCantidadCompra').modal('hide');
        var btn = document.getElementById(NombreModal);
        //modal.style.display = 'none';
        //modal.className = 'modal fade';
        btn.click();
    }
};
VentasComponent.ctorParameters = () => [
    { type: _services_servVenta_venta_service__WEBPACK_IMPORTED_MODULE_2__["VentaService"] },
    { type: _services_global_service__WEBPACK_IMPORTED_MODULE_3__["GlobalService"] }
];
VentasComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-ventas',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./ventas.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/ventas/ventas.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./ventas.component.css */ "./src/app/ventas/ventas.component.css")).default]
    })
], VentasComponent);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyCGsjlAIaL8ZzcvfErgxdQ5WvI4rxer570",
        authDomain: "apiz-e08b5.firebaseapp.com",
        databaseURL: "https://apiz-e08b5.firebaseio.com",
        projectId: "apiz-e08b5",
        storageBucket: "apiz-e08b5.appspot.com",
        messagingSenderId: "595489523923",
        appId: "1:595489523923:web:2a92c7dbe70fd7b924ec26"
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");





if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\WEB\Angular\frontZ\frontZ\sistemaz\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map